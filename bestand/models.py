from django.contrib.gis.db import models

from massnahmen.models import GemeindeBL


class Bestand(models.Model):
    ENTW_CHOICES = (
        ('1', 'Jungwuchs/Dickung'),
        ('2', 'übrige'),
        ('3', 'schwaches Stangenholz'),
        ('4', 'starkes Stangenholz'),
        ('5', 'schwaches Baumholz'),
        ('6', 'mittleres Baumholz'),
        ('7', 'starkes Baumholz'),
        ('8', 'Dauerwald'),
        ('9', 'übrige'),
        ('B', 'dauernd unbestockt'),
        ('U', 'dauernd unbestockt'),
    )
    gid = models.AutoField(primary_key=True)
    # Only "interesting" fields of the table are represented
    code = models.CharField(max_length=10, db_column="BNR")
    typ  = models.IntegerField(db_column="BESTCODE")
    jahr = models.IntegerField(db_column="AJAHR")
    entw_stufe = models.CharField(max_length=1, choices=ENTW_CHOICES, db_column="ENTW")
    flaeche = models.FloatField(db_column="Shape_Area")
    gemeinde = models.ForeignKey(GemeindeBL, to_field='gemeinde_i', null=True, blank=True,
        db_column="GDE", on_delete=models.CASCADE)

    the_geom = models.PolygonField(srid=2056, null=True)

    map_name = 'bestandeskarte'

    class Meta:
        db_table = 'Bestandeskarte'  # Old one was 'bk_schauenburg'

    def __str__(self):
        return "%s (Typ: %s, Gem: %s)" % (self.code, self.typ, self.gemeinde)


class Einheit(models.Model):
    gid = models.AutoField(primary_key=True)
    geom_id = models.IntegerField(db_column="GEOM_ID")
    gemeinde = models.CharField(max_length=255, db_column="GEMEINDE")
    flaeche_num = models.CharField(max_length=255, db_column="FLAECHENNU")
    einheit_code = models.CharField(max_length=255, db_column="EINHEIT_CD")
    einheit_text = models.CharField(max_length=255, db_column="EINHEIT_TE")
    farbwert = models.CharField(max_length=255, db_column="FARBWERT")
    id1 = models.IntegerField(db_column="ID1")
    ek = models.IntegerField()
    flaeche = models.FloatField()
    the_geom = models.PolygonField(srid=2056, null=True)

    map_name = 'einheitskarte'

    class Meta:
        db_table = 'ek_schauenburg'

    def __str__(self):
        return "%s (%s) - %s" % (self.gemeinde, self.geom_id, self.einheit_text)

    @property
    def code(self):
        return self.geom_id
