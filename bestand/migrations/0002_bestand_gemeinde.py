import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bestand', '0001_initial'),
        ('massnahmen', '__first__'),
    ]

    operations = [
        migrations.AddField(
            model_name='bestand',
            name='gemeinde',
            field=models.ForeignKey(
                blank=True, db_column='GDE', null=True, on_delete=django.db.models.deletion.CASCADE, to='massnahmen.gemeindebl', to_field='gemeinde_i'
            ),
        ),
    ]
