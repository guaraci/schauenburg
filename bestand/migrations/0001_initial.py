from django.contrib.gis.db.models.fields import PolygonField
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Bestand',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('code', models.CharField(db_column='BNR', max_length=10)),
                ('typ', models.IntegerField(db_column='BESTCODE')),
                ('jahr', models.IntegerField(db_column='AJAHR')),
                ('entw_stufe', models.CharField(choices=[
                    ('1', 'Jungwuchs/Dickung'), ('2', 'übrige'), ('3', 'schwaches Stangenholz'), ('4', 'starkes Stangenholz'), ('5', 'schwaches Baumholz'), ('6', 'mittleres Baumholz'), ('7', 'starkes Baumholz'), ('8', 'Dauerwald'), ('9', 'übrige'), ('B', 'dauernd unbestockt'), ('U', 'dauernd unbestockt')
                ], db_column='ENTW', max_length=1)),
                ('flaeche', models.FloatField(db_column='Shape_Area')),
                ('the_geom', PolygonField(null=True, srid=2056)),
            ],
            options={
                'db_table': 'Bestandeskarte',
            },
        ),
        migrations.CreateModel(
            name='Einheit',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('geom_id', models.IntegerField(db_column='GEOM_ID')),
                ('gemeinde', models.CharField(db_column='GEMEINDE', max_length=255)),
                ('flaeche_num', models.CharField(db_column='FLAECHENNU', max_length=255)),
                ('einheit_code', models.CharField(db_column='EINHEIT_CD', max_length=255)),
                ('einheit_text', models.CharField(db_column='EINHEIT_TE', max_length=255)),
                ('farbwert', models.CharField(db_column='FARBWERT', max_length=255)),
                ('id1', models.IntegerField(db_column='ID1')),
                ('ek', models.IntegerField()),
                ('flaeche', models.FloatField()),
                ('the_geom', PolygonField(null=True, srid=2056)),
            ],
            options={
                'db_table': 'ek_schauenburg',
            },
        ),
    ]
