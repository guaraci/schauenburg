from django.contrib.gis import admin

from .models import Bestand, Einheit

admin.site.register(Bestand, admin.GISModelAdmin)
admin.site.register(Einheit, admin.GISModelAdmin)
