from django.urls import path

from .views import LayerView, MapView

urlpatterns = [
    path('map/', MapView.as_view(), name='public-map'),
    path('layer/<layername>/', LayerView.as_view(), name='public-layer'),
]
