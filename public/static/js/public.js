"use strict";

var map = null;
var currentLayer = null;

const baumartMapping = {
    tot: 'Totholz',
    ei: 'Eiche',
    eu: 'Efeu',
    FAh: 'Feldahorn',
    FO: 'Föhre',
    BAh: 'Bergahorn',
    wt: 'Weisstanne',
    fö: 'Föhre',
    li: 'Linde',
    PA: 'Papel',
    Ta: 'Weisstanne',
    bu: 'Buche',
    SpA: 'Spitzahorn',
    fi: 'Fichte',
    BAH: 'Bergahorn',
    we: 'Weide',
    haB: 'Hagenbuche',
    el: 'Elsbeere',
    nu: 'Nussbaum',
    ki: 'Kirsch',
    es: 'Esche',
    SAH: 'Spitzahorn',
    unbekannt: 'unbekannt'
};

function layerClicked(ev, map, layer) {
    const props = layer.feature.properties;
    const popupMapping = {
        'Planung': planungPopup,
        'Pflege': pflegePopup,
        'Pflanzung': pflanzungPopup,
        'Freizeiteinrichtung': einrichtungPopup,
        'WaldrandPflegeKonzept': waldrandPopup,
        'AltholzInseln': altholzPopup,
        'HabitatBaum': habitatPopup,
        'Bestand': bestandPopup
    };
    const popupFunc = popupMapping[props.model];
    const popupText = popupFunc ? popupFunc(props) : '';
    layer.bindPopup(popupText).openPopup();
}

function planungPopup(props) {
    const geplant = props.quartal_geplant || props.geplant;
    let popText = (
        `Lokalname: ${props.lokalname}<br>
        Waldeigentümer: ${props.eigentuemer.replace('BG ', 'Bürgergemeinde ')}<br>
        Eingriffszeitpunkt: ${geplant}`
    )
    if (props.eingriffsz) popText += `<br>Eingriffsziel: ${props.eingriffsz}`;
    const flaeche = props.flaeche.toLocaleString('de-CH');
    popText += `
        <br>Holzvolumen geschätzt: ${props.geschaetzt.toLocaleString('de-CH')} m<sup>3</sup><br>
        Eingriffsfläche: ${flaeche} m<sup>2</sup>`;
    const schlagart = [
        [props.waldbewirtschaftung, 'Waldbewirtschaftung'], [props.zwangsnutzung, 'Zwangsnutzung'],
        [props.naturschutz, 'Naturschutz'], [props.sicherheitseingriff, 'Sicherheitseingriff']
    ].map(tpl => tpl[0] ? tpl[1] : null).filter(Boolean);
    schlagart.push(props.typ);
    popText += `<br>Eingriffsgrund: ${schlagart.filter(Boolean).join(", ")}`;
    return popText;
}

function pflegePopup(props) {
    const flaeche = props.flaeche.toLocaleString('de-CH');
    let popText = (
        `Lokalname: ${props.lokalname}<br>
        Waldeigentümer: ${props.eigentum.replace('BG ', 'Bürgergemeinde ')}<br>
        Eingriffsfläche: ${flaeche}m<sup>2</sup><br>
        Pflegeziel: ${props.pflegeziel}<br>
        Bestockungsziel: ${props.bestockungsziel}`
    )
    return popText;
}

function pflanzungPopup(props) {
    const baumarten = props.baumarten_list;
    const flaeche = props.flaeche.toLocaleString('de-CH');
    let popText = (
        `Lokalname: ${props.lokalname} <br>
        Waldeigentümer: ${props.eigentuemer.replace('BG ', 'Bürgergemeinde ')}<br>
        ${flaeche}m<sup>2</sup><br>
        Realisiert: ${props.realisiert}<br>
        Baumarten: ${baumarten.join(", ")}`
    )
    return popText;
}

function einrichtungPopup(props) {
    return `${props.name}, ${props.obj_art}`;
}

function waldrandPopup(props) {
    return `Länge: ${props.length}m<br>${props.waldrandty}`;
}

function waldrandStyle(feature) {
    let color = '#3388ff';  // Leaflet default
    switch(feature.properties.waldrandty_raw) {
        case 1:
            color = '#ff0000'; break;
        case 2:
            color = '#ffa500'; break;
        case 3:
            color = '#e7e700'; break;
        case 4:
            color = '#235dae'; break;
        case 5:
            color = '#8fbeff'; break;
        case 6:
            color = '#ffffff'; break;
    }
    return {color: color};
}

function altholzPopup(props) {
    const flaeche = props.flaeche.toLocaleString('de-CH');
    let popText = (
        `Lokalname: ${props.lokalname}<br>
        Waldeigentümer: ${props.eigentum.replace('BG ', 'Bürgergemeinde ')}`
    );
    if (props.schutzstatus) popText += `<br>Schutzstatus: ${props.schutzstatus}`;
    popText += `<br>Fläche: ${flaeche}m<sup>2</sup>`;
    return popText;
}

function habitatPopup(props) {
    let popText = baumartMapping[props.baumart];
    if (props.bhd) popText += `, ø ${props.bhd}cm`;
    return popText;
}

function bestandPopup(props) {
    return `Entwicklungstufe: ${props.entw_stufe}`;
}

function einrichtungMarker(feature, latlng) {
    var icons = JSON.parse(document.getElementById('icon-urls').innerHTML);
    switch(feature.properties.obj_art) {
        case 'Bänkli':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.bankli, iconSize: [28, 28]})});
        case 'Waldhütte':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.waldhutte, iconSize: [28, 28]})});
        case 'Waldinfo Point':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.information, iconSize: [28, 28]})});
        case 'Brunnen':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.brunnen, iconSize: [28, 28]})});
        case 'Waldspielgruppe/Waldkindergarten':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.spielplatz, iconSize: [28, 28]})});
        case 'Sportanlage':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.sport, iconSize: [28, 28]})});
        case 'Grillstelle':
            return L.marker(latlng, {icon: L.icon({iconUrl: icons.feuerstelle, iconSize: [28, 28]})});
        case 'Archeologisches Objekt':
            return L.circleMarker(latlng, Object.assign({}, geojsonMarkerOptions, {fillColor: '#ffa500'}));
        case 'andere':
            return L.circleMarker(latlng, Object.assign({}, geojsonMarkerOptions, {fillColor: '#8B6914'}));
        default:
            return L.circleMarker(latlng, Object.assign({}, geojsonMarkerOptions, {fillColor: '#aaaaaa'}));
    }
}

function loadError(err) {
    console.log(err);
    document.querySelector('#err-window').classList.remove('hidden');
}

function afterLayerLoaded(layer) {
    currentLayer = layer;
    if (layer === null) document.querySelector('#no-objects').classList.remove('hidden');
    else document.querySelector('#no-objects').classList.add('hidden');
}

function loadLayer(layerKey) {
    // TODO: maybe keep the layer to not reload from server at next request.
    if (currentLayer) map.map.removeLayer(currentLayer);
    document.querySelector('#err-window').classList.add('hidden');
    let options = {};
    if (layerKey == 'inventar_geschuetzter_naturobjekte') {
        // Note about getFeatureInfo: Leaflet doesn't support it without plugins. Could be added with:
        // - Plugin https://github.com/heigeo/leaflet.wms
        // - Custom impl couls be based on https://gist.github.com/rclark/6908938
        currentLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'inventar_geschuetzter_naturobjekte',
            minZoom: 18,
            maxZoom: 28,
            format: 'image/png',
            transparent: true,
            useCache: true
        });
        currentLayer.addTo(map.map);
        document.querySelector('#no-objects').classList.add('hidden');
    } else {
        const select = document.querySelector('#layer-select');
        const layerURL = select.selectedOptions[0].dataset.url;
        if (layerKey == 'habitat') {
            options.circleMarkerStyle = Object.assign({}, geojsonMarkerOptions, {fillColor: '#008000'});
        } else if (layerKey == 'waldrand') {
            options.style = waldrandStyle;
        } else if (layerKey == 'einrichtungen') {
            options.pointToLayer = einrichtungMarker;
        }
        document.querySelector('#waiting').classList.remove('hidden');
        map.loadFeaturesFromURL(layerURL, options, false, layerClicked)
            .then(afterLayerLoaded)
            .catch(loadError)
            .finally(() => {
                document.querySelector('#waiting').classList.add('hidden');
            });
    }
}

function setLayerFromQuerystring() {
    const searchParams = new URLSearchParams(document.location.search);
    const defaultLayerKey = 'holzschlag';
    let layerKey = searchParams.get('layer') || defaultLayerKey;
    const select = document.querySelector('#layer-select');
    const option = select.querySelector('[value="' + layerKey + '"]');
    if (!option) layerKey = defaultLayerKey;
    select.value = layerKey;
    loadLayer(layerKey);
}

document.addEventListener('DOMContentLoaded', (ev) => {
    const select = document.querySelector('#layer-select');
    map = new Map(['ortho', 'plan']);
    map.map.setZoom(20);
    setLayerFromQuerystring();
    select.addEventListener('change', (ev) => {
        loadLayer(select.value);
        history.pushState({}, '', window.location.pathname + `?layer=${select.value}`);
    });
    window.onpopstate = setLayerFromQuerystring;
});
