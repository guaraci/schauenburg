import json
from datetime import date

from django.contrib.gis.db.models.functions import Length
from django.contrib.gis.serializers.geojson import Serializer as GeoJSONSerializer
from django.http import Http404, JsonResponse
from django.views.generic import TemplateView, View

from bestand.models import Bestand
from massnahmen.models import AltholzInseln, HabitatBaum, Pflanzung, Pflege, Planung
from unterhalt.models import Freizeiteinrichtung
from waldrand.models import WaldrandPflegeKonzept

PUBLIC_WEB = "dev.forstrevier.cyon.site"


class PublicSerializer(GeoJSONSerializer):
    def handle_fk_field(self, obj, field):
        value = getattr(obj, field.name)
        if value:
            value = str(value)
        self._current[field.name] = value

    def handle_m2m_field(self, obj, field):
        self._current[field.name] = [str(subobj) for subobj in getattr(obj, field.name).all()]

    def _value_from_field(self, obj, field):
        if hasattr(obj, f'get_{field.name}_display'):
            self._current[f'{field.name}_raw'] = getattr(obj, field.name)
            return getattr(obj, f'get_{field.name}_display')()
        return super()._value_from_field(obj, field)

    def end_object(self, obj):
        geom_instance = getattr(obj, self.geometry_field)
        if 'LineString' in geom_instance.geom_type and hasattr(obj, 'length'):
            self._current['length'] = int(obj.length.m)
        elif 'Polygon' in geom_instance.geom_type:
            self._current['flaeche'] = int(geom_instance.area)
        self._current['model'] = obj.__class__.__name__
        if obj._meta.model == Planung:
            self._current['naturschutz'] = obj.naturschutz
            if obj.quartal_geplant:
                self._current['quartal_geplant'] = obj.get_quartal_geplant_display()
        super().end_object(obj)


class MapView(TemplateView):
    template_name = 'public/map.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'eingriffsjahr': current_eingriffsjahr(),
        }

    def render_to_response(self, *args, **response_kwargs):
        response = super().render_to_response(*args, **response_kwargs)
        response["Content-Security-Policy"] = "frame-ancestors %s" % PUBLIC_WEB
        return response


class LayerView(View):
    def get(self, request, layername=None, **kwargs):
        layer_method = {
            'planung': self.get_planung,
            'pflege': self.get_pflege,
            'pflanzung': self.get_pflanzung,
            'einrichtung': self.get_einrichtung,
            'waldrand': self.get_waldrand,
            'altholz': self.get_altholz,
            'habitat': self.get_habitat,
            'bestand': self.get_bestand,
        }.get(layername, None)
        if layer_method is None:
            raise Http404()
        serial = layer_method()
        return JsonResponse(json.loads(serial.getvalue()), safe=False)

    def serialize_objects(self, queryset, fields):
        serial = PublicSerializer()
        serial.serialize(queryset, fields=fields, srid=4326)
        return serial

    def get_planung(self):
        eingriffsjahr = current_eingriffsjahr()
        planungs = Planung.objects.filter(geplant__jahr=eingriffsjahr, the_geom__isnull=False)
        return self.serialize_objects(planungs, [
            # flaeche/naturschutz handled in end_object
            'the_geom', 'lokalname', 'eigentuemer', 'typ', 'geschaetzt', 'geplant',
            'eingriffsz', 'quartal_geplant', 'waldbewirtschaftung', 'zwangsnutzung',
            'sicherheitseingriff'
        ])

    def get_pflege(self):
        eingriffsjahr = current_eingriffsjahr()
        pfleges = Pflege.objects.filter(geplant__jahr=eingriffsjahr, the_geom__isnull=False)
        return self.serialize_objects(pfleges, [
            'the_geom', 'lokalname', 'eigentum', 'pflegeziel', 'bestockungsziel'
        ])

    def get_pflanzung(self):
        pflanzungen = Pflanzung.objects.filter(
            realisiert_jahre__jahr=current_eingriffsjahr(), geom__isnull=False
        ).prefetch_related('baumarten_list')
        return self.serialize_objects(pflanzungen, ['geom', 'lokalname', 'eigentuemer', 'realisiert', 'baumarten_list'])

    def get_einrichtung(self):
        einricht = Freizeiteinrichtung.objects.filter(the_geom__isnull=False).only('name', 'obj_art', 'the_geom')
        return self.serialize_objects(einricht, ['the_geom', 'name', 'obj_art'])

    def get_altholz(self):
        altholz = AltholzInseln.objects.all()
        return self.serialize_objects(altholz, ['geom', 'lokalname', 'eigentum', 'schutzstatus'])

    def get_habitat(self):
        habitat = HabitatBaum.objects.all()
        return self.serialize_objects(habitat, ['geom', 'baumart', 'bhd'])

    def get_waldrand(self):
        waldrand = WaldrandPflegeKonzept.objects.all().annotate(length=Length('geom'))
        return self.serialize_objects(waldrand, ['geom', 'length', 'waldrandty'])

    def get_bestand(self):
        bestand = Bestand.objects.exclude(the_geom__isnull=True)
        return self.serialize_objects(bestand, ['the_geom', 'entw_stufe'])


def active_year():
    """Return current year from April, but previous year from January until March."""
    return date.today().year if date.today().month >=4 else date.today().year - 1


def current_eingriffsjahr():
    """Return e.g. '2021/22'"""
    year = active_year()
    return f'{year}/{str(year + 1)[-2:]}'


def previous_eingriffsjahr():
    year = active_year()
    return f'{year - 1}/{str(year)[-2:]}'
