from datetime import date

from django.contrib.gis.geos import Polygon
from django.test import TestCase
from django.urls import reverse

from massnahmen.models import Eigentuemer, EingriffsJahr, MassnahmeTyp, Planung


class PublicTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # The pks are 'gid'
        cls.typ = MassnahmeTyp.objects.create(id=1, name='Räumung')
        eigent = Eigentuemer.objects.create(id=1, name='BG Muttenz')
        this_year = date.today().year -1 if date.today().month < 4 else date.today().year
        eingriffsjahr = f'{this_year}/{str(this_year + 1)[-2:]}'
        cls.this_year = EingriffsJahr.objects.create(id=1, jahr=eingriffsjahr)
        Planung.objects.create(
            typ=cls.typ,
            lokalname="Die Name",
            geschaetzt=320,
            bemerkunge='',
            eingriffsz="Stabiles, artenreiches Baumholz",
            the_geom=Polygon(
                ((2619513, 1261700),
                 (2619552, 1261700),
                 (2619527, 1261664),
                 (2619513, 1261700)),
                 srid=2056
            ),
            eigentuemer=eigent,
            zwangsnutzung=True,
            geplant=cls.this_year,
            quartal_geplant=3,
            schlagchef="Ueli",
            arbeitsauftrag="Die Auftrag",
        )

    def test_planung_layer(self):
        response = self.client.get(reverse('public-layer', args=['planung']))
        self.maxDiff = None
        self.assertEqual(
            response.json(),
            {
                'type': 'FeatureCollection',
                'crs': {'properties': {'name': 'EPSG:4326'}, 'type': 'name'},
                'features': [{
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Polygon',
                        'coordinates': [
                            [[7.697624138039357, 47.50576153687061],
                             [7.698141792559919, 47.50576038127114],
                             [7.697808388938028, 47.50543734442441],
                             [7.697624138039357, 47.50576153687061]]
                        ],
                    },
                    'id': Planung.objects.first().pk,
                    'properties': {
                        'lokalname': 'Die Name',
                        'eigentuemer': 'BG Muttenz',
                        'typ': 'Räumung',
                        'eingriffsz': 'Stabiles, artenreiches Baumholz',
                        'geplant': str(self.this_year),
                        'geschaetzt': 320,
                        'flaeche': 702,
                        'model': 'Planung',
                        'quartal_geplant': 'Jul-Sep (Q3)',
                        'quartal_geplant_raw': 3,
                        'naturschutz': False,
                        'sicherheitseingriff': False,
                        'waldbewirtschaftung': False,
                        'zwangsnutzung': True,
                    },
                }]
            }
        )
