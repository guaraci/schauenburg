from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('unterhalt', '0003_date_to_datetime'),
    ]

    operations = [
        migrations.AddField(
            model_name='kontaktperson',
            name='email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AddField(
            model_name='kontaktperson',
            name='tel',
            field=models.CharField(blank=True, max_length=25),
        ),
    ]
