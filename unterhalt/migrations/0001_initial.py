from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Eigentuemertyp',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'lt_eigentuemertyp',
            },
        ),
        migrations.CreateModel(
            name='Einrichtungsart',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'lt_einrichtungsart',
            },
        ),
        migrations.CreateModel(
            name='Freizeiteinrichtung',
            fields=[
                ('gid', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(db_column='objektname', max_length=30, blank=True, verbose_name='Flurname')),
                ('beschreibung', models.CharField(max_length=80)),
                ('unterhaltsmassnahmen', models.TextField(blank=True)),
                ('bemerkung_unterhalt', models.TextField(blank=True)),
                ('laufender_unterhalt', models.BooleanField(default=False)),
                ('periodischer_unterhalt', models.BooleanField(default=False)),
                ('the_geom', django.contrib.gis.db.models.fields.PointField(null=True, srid=2056, verbose_name="Standort")),
            ],
            options={
                'db_table': 'freizeiteinrichtungen',
            },
        ),
        migrations.CreateModel(
            name='Kontaktperson',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'lt_kontaktperson',
            },
        ),
        migrations.CreateModel(
            name='Unterhalt',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('datum', models.DateField()),
                ('massnahme', models.TextField()),
                ('umsetzung', models.IntegerField()),
                ('objekt', models.ForeignKey(to='unterhalt.Freizeiteinrichtung', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'unterhalt',
            },
        ),
        migrations.AddField(
            model_name='freizeiteinrichtung',
            name='kontaktperson',
            field=models.ForeignKey(to='unterhalt.Kontaktperson', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='freizeiteinrichtung',
            name='obj_art',
            field=models.ForeignKey(to='unterhalt.Einrichtungsart', on_delete=models.CASCADE, verbose_name="Objekttyp"),
        ),
        migrations.AddField(
            model_name='freizeiteinrichtung',
            name='waldbesitzer',
            field=models.ForeignKey(to='massnahmen.Eigentuemer', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='freizeiteinrichtung',
            name='werkeigentuemer',
            field=models.ForeignKey(to='unterhalt.Eigentuemertyp', on_delete=models.CASCADE),
        ),
    ]
