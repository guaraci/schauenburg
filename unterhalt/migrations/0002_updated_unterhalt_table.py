from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('unterhalt', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='unterhalt',
            options={'ordering': ('-datum',)},
        ),
        migrations.RenameField(
            model_name='unterhalt',
            old_name='massnahme',
            new_name='massnahme_objekt',
        ),
        migrations.AlterField(
            model_name='unterhalt',
            name='massnahme_objekt',
            field=models.TextField(blank=False),
        ),
        migrations.AlterField(
            model_name='unterhalt',
            name='umsetzung',
            field=models.IntegerField(choices=[(1, 'Objektbesitzer'), (2, 'Forstbetrieb'), (3, 'Baumpfleger'), (4, 'Einwohnergemeinde')], help_text='Unterhaltsarbeiten auszuführen durch', null=True),
        ),
        migrations.AlterField(
            model_name='freizeiteinrichtung',
            name='beschreibung',
            field=models.CharField(blank=True, max_length=80),
        ),
        migrations.AddField(
            model_name='unterhalt',
            name='erledigt',
            field=models.DateField(blank=True, help_text='Unterhaltsarbeiten erledigt am (Datum)', null=True),
        ),
        migrations.AddField(
            model_name='unterhalt',
            name='massnahmen_baumbestand',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='unterhalt',
            name='unterhalt_baumbestand',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='unterhalt',
            name='unterhalt_objekt',
            field=models.BooleanField(default=True),
        ),
    ]
