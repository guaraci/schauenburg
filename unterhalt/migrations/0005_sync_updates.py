from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    # Fake applied
    dependencies = [
        ('unterhalt', '0004_kontakt_email_tel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unterhalt',
            name='massnahme_objekt',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='unterhalt',
            name='umsetzung',
            field=models.IntegerField(blank=True, choices=[(1, 'Objektbesitzer'), (2, 'Forstbetrieb'), (3, 'Baumpfleger'), (4, 'Einwohnergemeinde')], help_text='Unterhaltsarbeiten auszuführen durch', null=True),
        ),
        migrations.CreateModel(
            name='Belegung',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('schulklasse', models.TextField()),
                ('infrastruktur', models.TextField(blank=True)),
                ('bemerkungen', models.TextField(blank=True)),
                ('einzeltage', models.DateTimeField(blank=True, null=True)),
                ('wochentage', models.TextField(blank=True)),
                ('tel_verant_pers', models.CharField(blank=True, max_length=100, verbose_name='Telefon verantwortliche Person')),
                ('email_verant_pers', models.CharField(blank=True, max_length=200, verbose_name='Email verantwortliche Person')),
                ('objekt', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='unterhalt.Freizeiteinrichtung')),
            ],
            options={
                'db_table': 'lt_belegung',
            },
        ),
    ]
