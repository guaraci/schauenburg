from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('unterhalt', '0007_unterhalt_verbose_names'),
    ]

    operations = [
        migrations.AddField(
            model_name='freizeiteinrichtung',
            name='foto',
            field=models.ImageField(blank=True, upload_to='fotos', verbose_name='Foto'),
        ),
        migrations.AddField(
            model_name='unterhalt',
            name='foto',
            field=models.ImageField(blank=True, upload_to='fotos', verbose_name='Foto'),
        ),
    ]
