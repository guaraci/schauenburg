from django.urls import path

from. import views

urlpatterns = [
    path('', views.FreizeitMapView.as_view(), name='freizeit-map'),
    path('massnahmen/', views.FreizeitMassnahmenView.as_view(), name='freizeit-massnahmen'),
    path('details/', views.FreizeitDetails.as_view(), name='freizeit-details'),
    path('edit/<int:pk>/', views.FreizeitEditView.as_view(), name='freizeit-edit'),
    path('delete/<int:pk>/', views.FreizeitDeleteView.as_view(), name='freizeit-delete'),
    path('add/', views.FreizeitAddView.as_view(), name='freizeit-add'),
    path('massnahme/<int:pk>/add/', views.UnterhaltAddView.as_view(),
        name='massnahme-add'),
    path('massnahme/<int:pk>/edit/', views.UnterhaltEditView.as_view(),
        name='massnahme-edit'),
    path('massnahme/<int:pk>/delete/', views.UnterhaltDeleteView.as_view(),
        name='massnahme-delete'),
    path('massnahme/<int:pk>/print/', views.MassnahmePDFView.as_view(),
        name='massnahme-pdf'),
    path('belegung/<int:pk>/add/', views.BelegungAddView.as_view(),
        name='belegung-add'),
    path('belegung/<int:pk>/edit/', views.BelegungEditView.as_view(),
        name='belegung-edit'),
    path('belegung/<int:pk>/delete/', views.BelegungDeleteView.as_view(),
        name='belegung-delete'),
]
