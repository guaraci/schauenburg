from datetime import date

from django import forms
from django.conf import settings
from django.contrib.gis.forms import OSMWidget
from leaflet.forms.widgets import LeafletWidget

from .models import Belegung, Freizeiteinrichtung, Unterhalt


class BaselOSMWidget(OSMWidget):
    default_lat = settings.DEFAULT_LAT
    default_lon = settings.DEFAULT_LON
    default_zoom = 13


class PickDateWidget(forms.DateInput):
    class Media:
        js = [
            'admin/js/core.js',
            'admin/js/calendar.js',
            'admin/js/admin/DateTimeShortcuts.js',
        ]

    def __init__(self, attrs=None, **kwargs):
        attrs = {'class': 'vDateField', 'size': '10', **(attrs or {})}
        super().__init__(attrs=attrs, **kwargs)


class FreizeitForm(forms.ModelForm):
    class Meta:
        model = Freizeiteinrichtung
        exclude = ['laufender_unterhalt', 'periodischer_unterhalt']
        widgets = {
            'the_geom': LeafletWidget,
            'foto': forms.ClearableFileInput(attrs={'accept': "image/*"}),
        }


class UnterhaltForm(forms.ModelForm):
    class Meta:
        model = Unterhalt
        fields = '__all__'
        widgets = {
            'objekt': forms.HiddenInput,
            'erledigt': PickDateWidget,
            'foto': forms.ClearableFileInput(attrs={'accept': "image/*"}),
        }
        labels = {
            'erledigt': "Erledigt am",
        }

    def clean_datum(self):
        datum = self.cleaned_data.get('datum')
        if datum and datum.year < date.today().year:
            raise forms.ValidationError("Es ist nicht möglich, ein Datum aus den letzten Jahren einzugeben")
        return datum


class BelegungForm(forms.ModelForm):
    class Meta:
        model = Belegung
        fields = '__all__'
        widgets = {
            'objekt': forms.HiddenInput,
            'einzeltage': PickDateWidget,
        }
