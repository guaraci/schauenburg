from datetime import date

from django.contrib.gis.db.models.functions import Transform
from django.db.models import Prefetch
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, DeleteView, DetailView, TemplateView, UpdateView

from massnahmen.pdf_output import PlanungPDF
from massnahmen.utils import GeoJSONSerializer

from .forms import BelegungForm, FreizeitForm, UnterhaltForm
from .models import Belegung, Freizeiteinrichtung, Unterhalt


class FreizeitSerializer(GeoJSONSerializer):
    def get_dump_object(self, obj):
        data = super().get_dump_object(obj)
        if 'massn_this_year' in self.selected_fields:
            all_ok = all([mass.erledigt is True or mass.kein_unterhalt is True for mass in obj.massn_this_year])
            data['properties']['this_year'] = 'red' if not obj.massn_this_year else ('green' if all_ok else 'orange')
        return data


class FreizeitMapView(TemplateView):
    template_name = 'freizeit_map.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query = Freizeiteinrichtung.objects.all().prefetch_related(Prefetch(
            'unterhalt_set',
            queryset=Unterhalt.objects.filter(datum__year=date.today().year),
            to_attr='massn_this_year'
        ))
        s = FreizeitSerializer()
        s.serialize(query, fields=('pk', 'name', 'massn_this_year', 'the_geom'), srid=4326)
        context.update({
            'title': 'Freizeiteinrichtungen',
            'features': s.getvalue(),
        })
        return context


class FreizeitMassnahmenView(TemplateView):
    template_name = 'freizeit_massnahmen.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Freizeiteinrichtungen Massnahmenübersicht',
            'unterhalt': Unterhalt.objects.filter(kein_unterhalt=False, erledigt__isnull=True),
        })
        return context


class FreizeitDetails(DetailView):
    template_name = 'freizeit_details.html'
    model = Freizeiteinrichtung

    def get_object(self, queryset=None):
        return Freizeiteinrichtung.objects.get(pk=self.request.GET.get('pk'))


class FreizeitEditView(UpdateView):
    template_name = 'freizeit_edit.html'
    model = Freizeiteinrichtung
    form_class = FreizeitForm

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'breadcrumb': [("Freizeiteinrichtungen", reverse('freizeit-map'))],
            'title': '%s bearbeiten' % self.object,
        }

    def get_success_url(self):
        return reverse('freizeit-map') + '#%s' % self.object.pk


class FreizeitAddView(CreateView):
    template_name = 'freizeit_edit.html'
    model = Freizeiteinrichtung
    form_class = FreizeitForm

    def get_success_url(self):
        return self.object.get_absolute_url()

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'breadcrumb': [("Freizeiteinrichtungen", reverse('freizeit-map'))],
            'title': 'Neue Freizeiteinrichtung',
        }


class FreizeitDeleteView(DeleteView):
    model = Freizeiteinrichtung
    success_url = reverse_lazy('freizeit-map')


class UnterhaltAddView(CreateView):
    model = Unterhalt
    form_class = UnterhaltForm
    template_name = 'unterhalt_edit.html'

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'objekt': Freizeiteinrichtung.objects.get(pk=self.kwargs['pk']),
            'datum': timezone.now(),
        })
        return initial

    def get_success_url(self):
        return self.object.objekt.get_absolute_url()


class UnterhaltEditView(UpdateView):
    model = Unterhalt
    form_class = UnterhaltForm
    template_name = 'unterhalt_edit.html'

    def get_object(self):
        obj = super().get_object()
        if not obj.editable:
            raise Http404("Sorry, not editable.")
        return obj

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'breadcrumb': [("Massnahmenübersicht", reverse('freizeit-massnahmen'))],
            'title': '%s bearbeiten' % self.object,
        }

    def get_success_url(self):
        return self.object.objekt.get_absolute_url()


class UnterhaltDeleteView(DeleteView):
    model = Unterhalt
    success_url = reverse_lazy('freizeit-map')

    def get_object(self):
        obj = super().get_object()
        if not obj.editable:
            raise Http404("Sorry, not editable.")
        return obj


class BelegungAddView(CreateView):
    model = Belegung
    form_class = BelegungForm
    template_name = 'belegung_edit.html'

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'objekt': Freizeiteinrichtung.objects.get(pk=self.kwargs['pk']),
        })
        return initial

    def get_success_url(self):
        return self.object.objekt.get_absolute_url()


class BelegungEditView(UpdateView):
    model = Belegung
    form_class = BelegungForm
    template_name = 'belegung_edit.html'

    def get_success_url(self):
        return self.object.objekt.get_absolute_url()


class BelegungDeleteView(DeleteView):
    model = Belegung
    success_url = reverse_lazy('freizeit-map')


class MassnahmePDF(PlanungPDF):
    main_map_on_first_page = False


class MassnahmePDFView(DetailView):
    model = Unterhalt
    queryset = Unterhalt.objects.all()

    def pdf_context(self, context):
        einricht = self.object.objekt
        context.update({
            'object_name': "Unterhalt",
            'subtitle': "Freizeiteinrichtungen - Massnahmen",
            'center': einricht.the_geom.centroid,
            'is_point': True,
            'detailed_map': False,
            'map_layer': 'freizeiteinrichtungen',
            'left_attrs': [
                ("Unterhaltsmassnahme", '', True),
                ("Objekt", self.object.massnahme_objekt, True),
                ("Baumbestand", self.object.massnahmen_baumbestand, True),
                ("Umsetzung", self.object.get_umsetzung_display(), True),
                ("", "<i>Der Übersichtsplan befindet sich auf der Rückseite.</i>", True),
            ],
            'right_attrs': [
                ("Objektart", str(einricht.obj_art)),
                ("Objektbeschreibung", einricht.beschreibung),
                ("Objektname", einricht.name),
                ("Kontaktperson", einricht.kontaktperson.full()),
                ("Werkeigentümer", str(einricht.werkeigentuemer)),
                ("Waldbesitzer", str(einricht.waldbesitzer)),
            ],
            'foot_data': [
                ['Auftrag erteilt am:', 'Unterschrift Betriebsleiter:'],
                ['', ''],
                ['Auftrag erledigt am:', 'Unterschrift Ausführender:'],
            ],
        })
        return context

    def render_to_response(self, context, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % slugify(str(self.object))
        MassnahmePDF(self.object.objekt).produce_pdf(response, self.pdf_context(context))
        return response
