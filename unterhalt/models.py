from datetime import date
from django.contrib.gis.db import models as gis_models
from django.db import models
from django.urls import reverse

from massnahmen.models import Eigentuemer


class Einrichtungsart(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'lt_einrichtungsart'

    def __str__(self):
        return self.name


class Eigentuemertyp(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'lt_eigentuemertyp'

    def __str__(self):
        return self.name


class Kontaktperson(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(blank=True)
    tel = models.CharField(max_length=25, blank=True)

    class Meta:
        db_table = 'lt_kontaktperson'

    def __str__(self):
        return self.name

    def full(self):
        """Return "name (tel, email)"."""
        full_name = self.name
        if self.email or self.tel:
            full_name = '{} ({})'.format(
                full_name, ', '.join([el for el in [self.tel, self.email] if el])
            )
        return full_name


class Freizeiteinrichtung(gis_models.Model):
    gid = models.AutoField(primary_key=True)
    name = models.CharField("Flurname", max_length=30, blank=True, db_column='objektname')
    beschreibung = models.CharField(max_length=80, blank=True)
    obj_art = models.ForeignKey(Einrichtungsart, on_delete=models.CASCADE, verbose_name="Objekttyp")
    werkeigentuemer = models.ForeignKey(Eigentuemertyp, on_delete=models.CASCADE)
    waldbesitzer = models.ForeignKey(Eigentuemer, on_delete=models.CASCADE)
    kontaktperson = models.ForeignKey(Kontaktperson, on_delete=models.CASCADE)
    unterhaltsmassnahmen = models.TextField(blank=True)
    bemerkung_unterhalt = models.TextField(blank=True)
    laufender_unterhalt = models.BooleanField(default=False)
    periodischer_unterhalt = models.BooleanField(default=False)
    foto = models.ImageField("Foto", upload_to='fotos', blank=True)

    the_geom = gis_models.PointField(srid=2056, null=True, verbose_name="Standort")

    class Meta:
        db_table = 'freizeiteinrichtungen'

    def __str__(self):
        return self.name or self.beschreibung or (
            self.obj_art.name if self.obj_art_id else 'unbekannt Einrichtung'
        )

    @property
    def title(self):
        return str(self)

    def get_absolute_url(self):
        return '%s#%d' % (reverse('freizeit-map'), self.pk)


UMSETZUNG_CHOICES = (
    (1, "Objektbesitzer"),
    (2, "Forstbetrieb"),
    (3, "Baumpfleger"),
    (4, "Einwohnergemeinde"),
)

class Unterhalt(models.Model):
    # datum/erledigt as DateTime Field because of a bug in QGIS Offline plugin
    objekt = models.ForeignKey(Freizeiteinrichtung, on_delete=models.CASCADE)
    datum = models.DateTimeField()
    kein_unterhalt = models.BooleanField("kein Unterhalt nötig", default=False,
        help_text="keine Unterhaltsmassnahmen am Objekt und am Baumbestand nötig")
    unterhalt_objekt = models.BooleanField("Unterhalt Object", default=True)
    massnahme_objekt = models.TextField("Massnahmen an der Erholungseinrichtung (Objekt)", blank=True)
    unterhalt_baumbestand = models.BooleanField("Unterhalt Baumbestand", default=False)
    massnahmen_baumbestand = models.TextField("Massnahmen am Baumbestand", blank=True)
    umsetzung = models.IntegerField(null=True, blank=True, choices=UMSETZUNG_CHOICES,
        help_text="Unterhaltsarbeiten auszuführen durch")
    erledigt = models.DateTimeField(null=True, blank=True,
        help_text="Unterhaltsarbeiten erledigt am (Datum)")
    foto = models.ImageField("Foto", upload_to='fotos', blank=True)

    class Meta:
        db_table = 'unterhalt'
        ordering = ('-datum',)

    def __str__(self):
        return "%s für %s (%s)" % (self.massnahme_objekt, self.objekt, self.datum)

    @property
    def editable(self):
        return self.datum.year >= date.today().year


class Belegung(models.Model):
    objekt = models.ForeignKey(Freizeiteinrichtung, on_delete=models.CASCADE)
    schulklasse = models.TextField()
    infrastruktur = models.TextField(blank=True)
    bemerkungen = models.TextField(blank=True)
    einzeltage = models.DateTimeField(null=True, blank=True)
    wochentage = models.TextField(blank=True)
    verant_person = models.TextField(),
    tel_verant_pers = models.CharField("Telefon verantwortliche Person", max_length=100, blank=True)
    email_verant_pers = models.CharField("Email verantwortliche Person", max_length=200, blank=True)

    class Meta:
        db_table = 'lt_belegung'

    def __str__(self):
        return "{} ({})".format(self.schulklasse, self.wann)

    @property
    def wann(self):
        return self.einzeltage.strftime('%d.%m.%Y') if self.einzeltage else self.wochentage
