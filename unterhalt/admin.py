from django.contrib.gis import admin

from . import models
from .forms import BaselOSMWidget


@admin.register(models.Kontaktperson)
class Kontaktperson(admin.ModelAdmin):
    list_display = ['name', 'tel', 'email']


@admin.register(models.Belegung)
class BelegungAdmin(admin.ModelAdmin):
    list_display = ['schulklasse', 'objekt', 'wann']


@admin.register(models.Freizeiteinrichtung)
class FreizeiteinrichtungAdmin(admin.GISModelAdmin):
    list_display = ['name', 'obj_art', 'beschreibung', 'waldbesitzer', 'kontaktperson']
    gis_widget = BaselOSMWidget


admin.site.register(models.Einrichtungsart)
admin.site.register(models.Eigentuemertyp)
admin.site.register(models.Unterhalt)
