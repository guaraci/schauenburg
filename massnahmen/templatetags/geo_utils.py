from django import template

register = template.Library()

@register.filter
def as_flaeche(surf):
    """ Return surface in "are" units """
    try:
        return int(surf)/100.0
    except ValueError:
        return ''
