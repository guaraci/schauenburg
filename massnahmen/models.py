from django.contrib.gis.db import models as gis_models
from django.urls import reverse
from django.db import models


class Eigentuemer(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    name = models.TextField(db_column='eigentuemer')

    class Meta:
        db_table = 'lt_eigentuemer'

    def __str__(self):
        return self.name


class GemeindeBL(gis_models.Model):
    gid         = models.AutoField(primary_key=True)
    geom_id     = models.IntegerField(null=True, blank=True, db_column='GEOM_ID')
    name        = models.CharField(max_length=64, null=True, blank=True, db_column='GEM_NAME')
    genauigkeit = models.CharField(max_length=64, null=True, blank=True, db_column='GENAUIGKEI')
    gemeinde_i  = models.IntegerField(null=True, blank=True, db_column='GEMEINDE_I', unique=True)
    gem_nr_alt  = models.IntegerField(null=True, blank=True, db_column='GEM_NR_ALT', unique=True, db_index=True)
    gem_nr_a_1  = models.IntegerField(null=True, blank=True, db_column='GEM_NR_A_1')
    bezirk_id   = models.IntegerField(null=True, blank=True, db_column="BEZIRK_ID_")
    bezirk_name = models.CharField(max_length=64, null=True, blank=True, db_column='BEZIRK_NAM')
    wep         = models.IntegerField(null=True, blank=True, db_column="WEP")
    the_geom    = gis_models.PolygonField(srid=2056, null=True, blank=True)

    class Meta:
        db_table = 'Gemeinden_BL'
        ordering = ('name',)

    def __str__(self):
        return self.name


class Baumart(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    baumart = models.CharField(max_length=250)

    class Meta:
        db_table = 'lt_baumarten'

    def __str__(self):
        return self.baumart.strip()


class EingriffsJahr(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    jahr = models.TextField()

    class Meta:
        db_table = 'lt_eingriffsjahr'

    def __str__(self):
        return self.jahr


class Pflanzung(gis_models.Model):
    lokalname = models.TextField(blank=True)
    realisiert = models.IntegerField(null=True, db_column='pflanzungsdatum_realisiert')
    realisiert_jahre = models.ForeignKey(EingriffsJahr, blank=True, null=True,
        related_name='pflanzungen',
        verbose_name="Geschäftsjahr realisiert", on_delete=models.CASCADE)
    eigentuemer = models.ForeignKey(Eigentuemer, null=True, db_column='eigentum',
        on_delete=models.CASCADE)
    zaunlaenge = models.IntegerField(default=0, help_text="Länge das Zauns in Meter")
    bemerkungen = models.TextField(blank=True, db_column='bemerkungen_besonderes')
    geom = gis_models.PolygonField(srid=2056)
    baumarten_list = models.ManyToManyField(Baumart, through='PflanzungBaumart')

    class Meta:
        db_table = 'pflanzflaechen'

    def __str__(self):
        value = self.eigentuemer.name if self.eigentuemer else '<kein eigentümer>'
        if self.lokalname:
            value = "%s (%s)" % (value, self.lokalname)
        return value

    def get_absolute_url(self):
        return reverse('pflanzung_detail', args=[self.pk])

    @property
    def title(self):
        return 'Pflanzung; %s (%s) - %s' % (
            self.lokalname, self.realisiert or '-', self.eigentuemer
        )


class PflanzungBaumart(models.Model):
    pflanzung = models.ForeignKey(Pflanzung, db_column='pflae_id', on_delete=models.CASCADE)
    baumart = models.ForeignKey(Baumart, db_column='baumart', on_delete=models.CASCADE)
    anzahl = models.IntegerField(null=True)
    provenienz = models.TextField(blank=True)
    abstand_x = models.IntegerField(null=True, db_column='pflanzabstand x')
    abstand_y = models.IntegerField(null=True, db_column='pflanzabstand y')
    einzelschutz = models.IntegerField(null=True),
    bemerkungen = models.TextField(blank=True, db_column='bemerkungen_besonderes')

    class Meta:
        db_table = 'pflanzung_baumarten'

    def __str__(self):
        return "%s in %s (x:%s, y:%s)" % (
            self.baumart, self.pflanzung, self.abstand_x or '-', self.abstand_y or '-')


class MassnahmeTyp(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    name = models.TextField(db_column='name_massnahme')

    class Meta:
        db_table = 'lt_massnahme'

    def __str__(self):
        return self.name


class NaturschutzMassnahme(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    name = models.TextField(db_column='massnahme')
    ziel = models.TextField(blank=True)

    class Meta:
        db_table = 'lt_naturschutzmassnahme'

    def __str__(self):
        return self.name


class Planung(gis_models.Model): # Rename to Holzschlag?
    QUARTAL_CHOICES = (
        (2, 'Apr-Jun (Q2)'),
        (3, 'Jul-Sep (Q3)'),
        (4, 'Okt-Dez (Q4)'),
        (1, 'Jan-März (Q1)'),
    )
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True, null=True, blank=True)
    typ = models.ForeignKey(MassnahmeTyp, db_column='Massnahmen', verbose_name="Massnahme",
        on_delete=models.CASCADE)
    lokalname = models.CharField(max_length=255, blank=True)
    geschaetzt = models.IntegerField(null=True, verbose_name="Holzvolumen geschätzt (sv)")
    bemerkunge = models.CharField(max_length=255, blank=True, verbose_name="Bemerkungen")
    beschreibung = models.CharField(max_length=255, blank=True, verbose_name="Beschreibung (Grund/Ziel)")
    eingriffsz = models.CharField(max_length=255, blank=True, db_column='Eingriffsz', verbose_name="Eingriffsziel")
    the_geom = gis_models.PolygonField(srid=2056)
    eigentuemer = models.ForeignKey(Eigentuemer, null=True, db_column='eigentuemer',
        on_delete=models.CASCADE)
    zwangsnutzung = models.BooleanField(null=True, default=False)
    waldbewirtschaftung = models.BooleanField(null=True, default=False)
    sicherheitseingriff = models.BooleanField(null=True, default=False)
    schlagvorbereitung = models.BooleanField(null=True, default=False)
    naturschutzmassnahme = models.ForeignKey(NaturschutzMassnahme, blank=True, null=True,
        db_column='naturschutzmassnahme',on_delete=models.CASCADE)
    geplant = models.ForeignKey(EingriffsJahr, blank=True, null=True,
        db_column='eingriffsjahr_geplant', related_name='geplant_planung',
        verbose_name="Massnahme geplant", on_delete=models.CASCADE)
    quartal_geplant = models.SmallIntegerField(null=True, blank=True, choices=QUARTAL_CHOICES)
    ausgefuehrt = models.IntegerField(blank=True, null=True,
        db_column='eingriffsjahr_ausgefuehrt', verbose_name="Massnahme ausgeführt")
    schlagchef = models.TextField()
    arbeitsauftrag = models.TextField()

    class Meta:
        db_table = 'massnahmenplanung'

    def __str__(self):
        if self.id:
            return "%s (%s)" % (self.lokalname, self.id)
        else:
            return self.lokalname or str(self.gid)

    @property
    def title(self):
        return "%s: %s (%s); %s" % (
            self.typ, self.__str__(), self.geplant or '-', self.eigentuemer or '')

    @property
    def naturschutz(self):
        return bool(self.naturschutzmassnahme_id)

    def get_absolute_url(self):
        return reverse('planung_detail', args=[self.pk])


class PflegeTyp(models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True)
    pflegetyp = models.TextField()

    class Meta:
        db_table = 'lt_pflegetyp'

    def __str__(self):
        return self.pflegetyp


class Pflege(gis_models.Model):
    gid = models.AutoField(primary_key=True)
    id  = models.IntegerField(unique=True, null=True, blank=True)
    the_geom = gis_models.PolygonField(srid=2056)
    lokalname = models.TextField(blank=True)
    geplant = models.ForeignKey(EingriffsJahr, blank=True, null=True,
        db_column='eingriffsjahr_geplant', related_name='geplant_pflege',
        verbose_name="Pflege geplant", on_delete=models.CASCADE)
    pflegeziel = models.TextField(blank=True)
    pflegetyp = models.ForeignKey(PflegeTyp, db_column='pflegetyp', on_delete=models.CASCADE)
    bestockungsziel = models.TextField(blank=True)
    gesuchnummer = models.IntegerField(null=True, blank=True)
    bemerkungen = models.TextField(db_column="bemerkungen, Besonderheiten", blank=True)
    auszahlungsbetrag = models.IntegerField(null=True, blank=True)
    ansatz_pflegeeingriff = models.IntegerField(null=True, blank=True,
        verbose_name="Ansatz Pflegeeingriff")
    naechster_eingriff = models.IntegerField(null=True, blank=True, verbose_name="Nächster Eingriff",
        help_text="Nächster Eingriff vorgesehen im Jahr 20?? (z.b. \"2020\")")
    neigung = models.BooleanField(default=False, help_text="angekreuzt: Neigung >50%")
    baumart = models.BooleanField(default=False, help_text="angekreuzt: Fi, Ta, Dgl, Lä")
    wertastung = models.BooleanField(default=False, help_text="angekreuzt: Wertastung vorgesehen")
    eigentum = models.ForeignKey(Eigentuemer, db_column='eigentum', on_delete=models.CASCADE)
    ver_person = models.TextField(blank=True, verbose_name="Verantwortliche Person")

    class Meta:
        db_table = 'massnahme_pflege'

    def __str__(self):
        return "-".join([self.eigentum.name, self.lokalname or '', self.pflegeziel or ''])

    @property
    def title(self):
        return f"{self.pflegetyp}: {self.lokalname} ({self.geplant}); {self.eigentum}"

    def get_absolute_url(self):
        return reverse('pflege_detail', args=[self.pk])


class Naturschutzgebiet(gis_models.Model):
    SCHUTZ_CHOICES = (
        ('kantonal', 'kantonal'),
        ('kommunal', 'kommunal'),
    )
    lokalname = models.CharField(max_length=200)
    geom = gis_models.MultiPolygonField(srid=2056)
    schutzstatus = models.CharField(max_length=10, choices=SCHUTZ_CHOICES)
    schutzziel = models.TextField()

    class Meta:
        db_table = 'naturschutzgebiet'
        constraints = [
            models.CheckConstraint(
                name="naturschutzstatus_choices",
                check=models.Q(schutzstatus__in=['kantonal', 'kommunal']),
            ),
        ]


class AltholzInseln(gis_models.Model):
    geom = gis_models.MultiPolygonField(srid=2056)
    lokalname = models.CharField(max_length=80)
    bemerkunge = models.CharField(max_length=80, blank=True)
    eigentum = models.ForeignKey(Eigentuemer, db_column='besitzer', on_delete=models.CASCADE)
    schutzstatus = models.TextField(blank=True, db_column="Schutzstatus")
    #"Flaeche" numeric(10,3),

    class Meta:
        db_table = 'altholzninseln'
        managed = False


class HabitatBaum(gis_models.Model):
    geom = gis_models.MultiPointField(srid=2056)
    #baumcode bigint
    baumart = models.CharField(max_length=10)
    bhd = models.IntegerField("Durchmesser", blank=True, null=True)
    #foto1 numeric
    #foto2 numeric
    bemerkung = models.CharField(max_length=254, blank=True)
    #pratteln_7 character varying(200)
    #umfang character varying(254),
    #gemeinde character(15),
    #koordinaten integer

    class Meta:
        db_table = 'habitatbaume'
        managed = False
