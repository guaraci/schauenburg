from django.contrib.gis.serializers.geojson import Serializer


class GeoJSONSerializer(Serializer):
    def get_dump_object(self, obj):
        data = super().get_dump_object(obj)
        if 'pk' in data['properties']:
            # Setting feature id, as this is used to set ol3 Feature id
            data['id'] = data['properties']['pk']
        return data
