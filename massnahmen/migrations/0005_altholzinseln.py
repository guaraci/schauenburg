import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0004_remove_pflanzung_flaeche'),
    ]

    operations = [
        migrations.CreateModel(
            name='AltholzInseln',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056)),
                ('lokalname', models.CharField(max_length=80)),
                ('bemerkunge', models.CharField(blank=True, max_length=80)),
            ],
            options={
                'db_table': 'altholzninseln',
                'managed': False,
            },
        ),
    ]
