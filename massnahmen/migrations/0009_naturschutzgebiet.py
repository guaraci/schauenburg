import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0008_booleans_nullable'),
    ]

    operations = [
        migrations.CreateModel(
            name='Naturschutzgebiet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056)),
                ('lokalname', models.CharField(max_length=200)),
                ('schutzstatus', models.CharField(choices=[('kantonal', 'kantonal'), ('kommunal', 'kommunal')], max_length=10)),
                ('schutzziel', models.TextField()),
            ],
            options={
                'db_table': 'naturschutzgebiet',
            },
        ),
        migrations.AddConstraint(
            model_name='naturschutzgebiet',
            constraint=models.CheckConstraint(check=models.Q(('schutzstatus__in', ['kantonal', 'kommunal'])), name='naturschutzstatus_choices'),
        ),
    ]
