from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0009_naturschutzgebiet'),
    ]

    operations = [
        migrations.AddField(
            model_name='pflege',
            name='geplant',
            field=models.ForeignKey(blank=True, db_column='eingriffsjahr_geplant', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='geplant_pflege', to='massnahmen.eingriffsjahr', verbose_name='Pflege geplant'),
        ),
    ]
