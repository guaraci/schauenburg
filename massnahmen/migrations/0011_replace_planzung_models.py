from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0010_pflege_geplant'),
    ]

    operations = [
        migrations.DeleteModel('pflanzung'),
        migrations.DeleteModel('pflanzungbaumart'),

        migrations.CreateModel(
            name='Pflanzung',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lokalname', models.TextField(blank=True)),
                ('realisiert', models.IntegerField(db_column='pflanzungsdatum_realisiert', null=True)),
                ('zaunlaenge', models.IntegerField(default=0, help_text='Länge das Zauns in Meter')),
                ('bemerkungen', models.TextField(blank=True, db_column='bemerkungen_besonderes')),
                ('eigentuemer', models.ForeignKey(db_column='eigentum', null=True, on_delete=models.deletion.CASCADE, to='massnahmen.eigentuemer')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(null=True, srid=2056)),
            ],
            options={
                'db_table': 'pflanzflaechen',
            },
        ),
        migrations.CreateModel(
            name='PflanzungBaumart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('anzahl', models.IntegerField(null=True)),
                ('provenienz', models.TextField(blank=True)),
                ('abstand_x', models.IntegerField(db_column='pflanzabstand x', null=True)),
                ('abstand_y', models.IntegerField(db_column='pflanzabstand y', null=True)),
                ('bemerkungen', models.TextField(blank=True, db_column='bemerkungen_besonderes')),
                ('baumart', models.ForeignKey(db_column='baumart', on_delete=models.deletion.CASCADE, to='massnahmen.baumart')),
                ('pflanzung', models.ForeignKey(db_column='pflae_id', on_delete=models.deletion.CASCADE, to='massnahmen.pflanzung')),
            ],
            options={
                'db_table': 'pflanzung_baumarten',
            },
        ),
        migrations.AddField(
            model_name='pflanzung',
            name='baumarten_list',
            field=models.ManyToManyField(through='massnahmen.PflanzungBaumart', to='massnahmen.baumart'),
        ),
    ]
