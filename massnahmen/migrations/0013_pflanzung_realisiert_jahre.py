from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0012_geom_fields_not_null'),
    ]

    operations = [
        migrations.AddField(
            model_name='pflanzung',
            name='realisiert_jahre',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=models.deletion.CASCADE, related_name='pflanzungen',
                to='massnahmen.eingriffsjahr', verbose_name='Geschäftsjahr realisiert'),
        ),
    ]
