from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0007_planung_beschreibung'),
    ]

    operations = [
        migrations.AlterField(
            model_name='planung',
            name='schlagvorbereitung',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='planung',
            name='sicherheitseingriff',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='planung',
            name='waldbewirtschaftung',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name='planung',
            name='zwangsnutzung',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
