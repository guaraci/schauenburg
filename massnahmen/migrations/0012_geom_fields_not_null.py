import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0011_replace_planzung_models'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pflanzung',
            name='geom',
            field=django.contrib.gis.db.models.fields.PolygonField(srid=2056),
        ),
        migrations.AlterField(
            model_name='pflege',
            name='the_geom',
            field=django.contrib.gis.db.models.fields.PolygonField(srid=2056),
        ),
        migrations.AlterField(
            model_name='planung',
            name='the_geom',
            field=django.contrib.gis.db.models.fields.PolygonField(srid=2056),
        ),
    ]
