from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0002_pflanzung_provenienz'),
    ]

    operations = [
        migrations.AddField(
            model_name='planung',
            name='quartal_geplant',
            field=models.SmallIntegerField(blank=True, choices=[(2, 'Apr-Jun (Q2)'), (3, 'Jul-Sep (Q3)'), (4, 'Okt-Dez (Q4)'), (1, 'Jan-März (Q1)')], null=True),
        ),
        migrations.AddField(
            model_name='planung',
            name='sicherheitseingriff',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='planung',
            name='waldbewirtschaftung',
            field=models.BooleanField(default=False),
        ),
    ]
