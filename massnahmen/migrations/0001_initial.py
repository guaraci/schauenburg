import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Baumart',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('baumart', models.CharField(max_length=250)),
            ],
            options={
                'db_table': 'lt_baumarten',
            },
        ),
        migrations.CreateModel(
            name='Eigentuemer',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('name', models.TextField(db_column='eigentuemer')),
            ],
            options={
                'db_table': 'lt_eigentuemer',
            },
        ),
        migrations.CreateModel(
            name='EingriffsJahr',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('jahr', models.TextField()),
            ],
            options={
                'db_table': 'lt_eingriffsjahr',
            },
        ),
        migrations.CreateModel(
            name='GemeindeBL',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('geom_id', models.IntegerField(blank=True, db_column='GEOM_ID', null=True)),
                ('name', models.CharField(blank=True, db_column='GEM_NAME', max_length=64, null=True)),
                ('genauigkeit', models.CharField(blank=True, db_column='GENAUIGKEI', max_length=64, null=True)),
                ('gemeinde_i', models.IntegerField(blank=True, db_column='GEMEINDE_I', null=True, unique=True)),
                ('gem_nr_alt', models.IntegerField(blank=True, db_column='GEM_NR_ALT', db_index=True, null=True, unique=True)),
                ('gem_nr_a_1', models.IntegerField(blank=True, db_column='GEM_NR_A_1', null=True)),
                ('bezirk_id', models.IntegerField(blank=True, db_column='BEZIRK_ID_', null=True)),
                ('bezirk_name', models.CharField(blank=True, db_column='BEZIRK_NAM', max_length=64, null=True)),
                ('wep', models.IntegerField(blank=True, db_column='WEP', null=True)),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'Gemeinden_BL',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='MassnahmeTyp',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('name', models.TextField(db_column='name_massnahme')),
            ],
            options={
                'db_table': 'lt_massnahme',
            },
        ),
        migrations.CreateModel(
            name='NaturschutzMassnahme',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('name', models.TextField(db_column='massnahme')),
                ('ziel', models.TextField(blank=True)),
            ],
            options={
                'db_table': 'lt_naturschutzmassnahme',
            },
        ),
        migrations.CreateModel(
            name='Pflanzung',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('geplant', models.IntegerField(db_column='pflanzdatum_geplant', null=True, verbose_name='Massnahme geplant')),
                ('realisiert', models.IntegerField(db_column='pflanzungsdatum_realisiert', null=True, verbose_name='Massnahme realisiert')),
                ('lokalname', models.TextField(blank=True)),
                ('wildschutz', models.BooleanField(default=False, help_text='Wildschutz nötig?')),
                ('zaunlaenge', models.IntegerField(default=0, help_text='Länge das Zauns in Meter')),
                ('einzelschutz', models.IntegerField(default=0, help_text='Anzahl Einzelschutz')),
                ('bemerkungen', models.TextField(blank=True, db_column='bemerkungen_besonderes')),
                ('pflanzabstand_x', models.IntegerField(blank=True, null=True)),
                ('pflanzabstand_y', models.IntegerField(blank=True, null=True)),
                ('baumarten', models.BooleanField(default=False, help_text='Baumarten angeben?')),
                ('flaeche', models.FloatField(null=True)),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(null=True, srid=2056)),
                ('baumart1', models.ForeignKey(blank=True, db_column='baumart1', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pflanzungen1', to='massnahmen.Baumart')),
                ('baumart2', models.ForeignKey(blank=True, db_column='baumart2', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pflanzungen2', to='massnahmen.Baumart')),
                ('baumart3', models.ForeignKey(blank=True, db_column='baumart3', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pflanzungen3', to='massnahmen.Baumart')),
            ],
            options={
                'db_table': 'pflanzungen',
            },
        ),
        migrations.CreateModel(
            name='PflanzungBaumart',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('abstand_x', models.IntegerField(db_column='pflanzabstand x', null=True)),
                ('abstand_y', models.IntegerField(db_column='pflanzabstand y', null=True)),
                ('provenienz', models.TextField(blank=True)),
                ('menge', models.IntegerField(null=True)),
                ('baumart', models.ForeignKey(db_column='baumart', on_delete=django.db.models.deletion.CASCADE, to='massnahmen.Baumart')),
                ('pflanzung', models.ForeignKey(db_column='fk_pflanzung', on_delete=django.db.models.deletion.CASCADE, to='massnahmen.Pflanzung')),
            ],
            options={
                'db_table': 'massnahme_pflanzung_baumarten',
            },
        ),
        migrations.CreateModel(
            name='Pflege',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(blank=True, null=True, unique=True)),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(null=True, srid=2056)),
                ('lokalname', models.TextField(blank=True)),
                ('eingriffsjahr', models.CharField(blank=True, db_column='planung_eingriffsjahr', help_text='Geplantes Eingriffsjahr', max_length=10)),
                ('pflegeziel', models.TextField(blank=True)),
                ('bestockungsziel', models.TextField(blank=True)),
                ('gesuchnummer', models.IntegerField(blank=True, null=True)),
                ('bemerkungen', models.TextField(blank=True, db_column='bemerkungen, Besonderheiten')),
                ('auszahlungsbetrag', models.IntegerField(blank=True, null=True)),
                ('ansatz_pflegeeingriff', models.IntegerField(blank=True, null=True, verbose_name='Ansatz Pflegeeingriff')),
                ('naechster_eingriff', models.IntegerField(blank=True, help_text='Nächster Eingriff vorgesehen im Jahr 20?? (z.b. "2020")', null=True, verbose_name='Nächster Eingriff')),
                ('neigung', models.BooleanField(default=False, help_text='angekreuzt: Neigung >50%')),
                ('baumart', models.BooleanField(default=False, help_text='angekreuzt: Fi, Ta, Dgl, Lä')),
                ('wertastung', models.BooleanField(default=False, help_text='angekreuzt: Wertastung vorgesehen')),
                ('ver_person', models.TextField(blank=True, verbose_name='Verantwortliche Person')),
                ('eigentum', models.ForeignKey(db_column='eigentum', on_delete=django.db.models.deletion.CASCADE, to='massnahmen.Eigentuemer')),
            ],
            options={
                'db_table': 'massnahme_pflege',
            },
        ),
        migrations.CreateModel(
            name='PflegeTyp',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(unique=True)),
                ('pflegetyp', models.TextField()),
            ],
            options={
                'db_table': 'lt_pflegetyp',
            },
        ),
        migrations.CreateModel(
            name='Planung',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField(blank=True, null=True, unique=True)),
                ('lokalname', models.CharField(blank=True, max_length=255)),
                ('geschaetzt', models.IntegerField(null=True, verbose_name='Holzvolumen geschätzt (sv)')),
                ('bemerkunge', models.CharField(blank=True, max_length=255, verbose_name='Bemerkungen')),
                ('eingriffsz', models.CharField(blank=True, db_column='Eingriffsz', max_length=255, verbose_name='Eingriffsziel')),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(null=True, srid=2056)),
                ('zwangsnutzung', models.BooleanField(default=False)),
                ('schlagvorbereitung', models.BooleanField(default=False)),
                ('ausgefuehrt', models.IntegerField(blank=True, db_column='eingriffsjahr_ausgefuehrt', null=True, verbose_name='Massnahme ausgeführt')),
                ('schlagchef', models.TextField()),
                ('arbeitsauftrag', models.TextField()),
                ('eigentuemer', models.ForeignKey(db_column='eigentuemer', null=True, on_delete=django.db.models.deletion.CASCADE, to='massnahmen.Eigentuemer')),
                ('geplant', models.ForeignKey(blank=True, db_column='eingriffsjahr_geplant', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='geplant_planung', to='massnahmen.EingriffsJahr', verbose_name='Massnahme geplant')),
                ('naturschutzmassnahme', models.ForeignKey(blank=True, db_column='naturschutzmassnahme', null=True, on_delete=django.db.models.deletion.CASCADE, to='massnahmen.NaturschutzMassnahme')),
                ('typ', models.ForeignKey(db_column='Massnahmen', on_delete=django.db.models.deletion.CASCADE, to='massnahmen.MassnahmeTyp', verbose_name='Massnahme')),
            ],
            options={
                'db_table': 'massnahmenplanung',
            },
        ),
        migrations.AddField(
            model_name='pflege',
            name='pflegetyp',
            field=models.ForeignKey(db_column='pflegetyp', on_delete=django.db.models.deletion.CASCADE, to='massnahmen.PflegeTyp'),
        ),
        migrations.AddField(
            model_name='pflanzung',
            name='baumarten_list',
            field=models.ManyToManyField(through='massnahmen.PflanzungBaumart', to='massnahmen.Baumart'),
        ),
        migrations.AddField(
            model_name='pflanzung',
            name='eigentuemer',
            field=models.ForeignKey(db_column='eigentum', null=True, on_delete=django.db.models.deletion.CASCADE, to='massnahmen.Eigentuemer'),
        ),
    ]
