from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0006_habitatbaum'),
    ]

    operations = [
        migrations.AddField(
            model_name='planung',
            name='beschreibung',
            field=models.CharField(blank=True, max_length=255, verbose_name='Beschreibung (Grund/Ziel)'),
        ),
    ]
