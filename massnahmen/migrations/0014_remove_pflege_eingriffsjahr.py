from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0013_pflanzung_realisiert_jahre'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pflege',
            name='eingriffsjahr',
        ),
    ]
