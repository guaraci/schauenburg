import copy
import os
import subprocess
import tempfile
from functools import partial
from time import strftime
from urllib.request import urlopen

from reportlab.platypus import (Paragraph, PageBreak, Table, TableStyle, Spacer,
    SimpleDocTemplate, Image as rlImage)
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm

from django.conf import settings
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.contrib.staticfiles import finders


def to_WGS84(point):
    ct = CoordTransform(SpatialReference(point.srid), SpatialReference(4326))
    return point.transform(ct, clone=True)


class PlanungPDF:
    main_map_on_first_page = True

    def __init__(self, obj):
        self.instance = obj
        self.styles = getSampleStyleSheet()
        self.style_n = self.styles['Normal']
        self.style_14 = copy.deepcopy(self.style_n)
        self.style_14.fontSize = 14
        self.style_14.leading = 16
        self.style_11 = copy.deepcopy(self.style_n)
        self.style_11.fontSize = 11
        self.style_11.leading = 13
        self.imgs = []  # Store image paths to remove at the end.
        # height cannot be none due to a bug in reportlab, setting it to big value so as
        # proportional calculation works
        self.header_image = rlImage(
            finders.find('img/logo-project.jpg'), width=8*cm, kind='proportional', height=1000,
            hAlign='LEFT',
        )

    def draw_footer(self, canvas, doc):
        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawString(2*cm, 1.5*cm, "Druckdatum: %s" % strftime('%d.%m.%Y, %H:%M'))
        canvas.restoreState()

    def get_map_image(self, center, map_layer, rate, scale, show_context=True, src='mapserver'):
        """ rate: width/height ratio of the image
            scale: pixel/map ratio of the map
        """
        if src != 'mapserver' or not settings.MAPFILE_PATH:
            # Get static image from http://staticmap.openstreetmap.de
            new_center = to_WGS84(center)
            cx, cy = new_center.coords
            try:
                img = urlopen(
                    "http://staticmap.openstreetmap.de/staticmap.php?center=%(y)s,%(x)s"
                    "&zoom=14&size=%(w)sx%(h)s&maptype=mapnik&markers=%(y)s,%(x)s,ol-marker" % {
                        'x': cx, 'y': cy, 'w': 1000, 'h': int(1000 * rate)
                    },
                    timeout=10
                )
            except Exception as err:
                raise RuntimeError(str(err)) from err
            tmp_img = tempfile.NamedTemporaryFile(suffix='.png', delete=False)
            tmp_img.write(img.read())
            tmp_img.close()
            self.imgs.append(tmp_img)
            return tmp_img.name
            #return os.path.join(settings.STATIC_ROOT, 'img', 'placeholder_map.png')
        else:
            # get the image name through an external script as mapscript is Python 2 only
            # TODO: this is no longer the case, map_image.py could be reintegrated to main code.
            if center.srid != 2056:
                center.transform(2056)
            cx, cy = center.coords
            cmd = [
                "python3", os.path.join(settings.PROJECT_PATH, "scripts", "map_image.py"),
                "--map-config=%s" % settings.MAPFILE_PATH, "--rate=%s" % rate,
                "--scale=%s" % scale, "--centerx=%s" % cx, "--centery=%s" % cy,
                "--layer=%s" % map_layer, "--pk=%d" % self.instance.pk,
                "--pkfield=%s" % self.instance._meta.pk.name,
            ]
            try:
                result = subprocess.check_output(cmd)
            except subprocess.CalledProcessError as err:
                raise RuntimeError('{}: {}'.format(cmd, err.output)) from err
            return result

    def produce_map(self, story, context):
        try:
            story.append(rlImage(
                self.get_map_image(context['center'], context['map_layer'], 0.5, 1.8, show_context=False),
                width=17*cm, height=8.5*cm
            ))
        except RuntimeError as err:
            story.append(Paragraph("Generating the map produced an error: %s" % err, self.style_n))
        style_coord = copy.deepcopy(self.style_n)
        style_coord.textColor = colors.red
        style_coord.fontSize = 14
        prefix = 'Mittlere ' if not context.get('is_point', False) else ''
        story.append(Paragraph(
            '<para alignment="right"><b>%sKoordinaten: %s</b></para>' % (
                prefix,
                ", ".join(map(lambda f:str(int(f)), context['center'].coords)),
            ),
            style_coord))

    def append_notfall_table(self, story):
        style_sm = copy.deepcopy(self.style_n)
        style_sm.fontSize=10
        P = partial(Paragraph, style=self.style_11)
        story.append(P("<b><u>Notfallorganisation</u></b>"))
        story.append(Spacer(0, 0.3*cm))
        notfall_data = [
            (P("<b>Notfallnummer</b>"), P("<b>112</b>"),
             P("<b>Was ist geschehen?</b><br/><i>-kurzer Situationsbericht</i>")),
            (P("<b>Ambulanz</b>"), P("<b>144</b>"), P("<b>Wer meldet?</b><br/><i>-Name, Standort, Erreichbarkeit</i>")),
            (P("<b>Rega</b>"), P("<b>1414</b>"),
             P("<b>Patient?</b><br/><i>-Alter, Verletzung, Bewusstsein, Atmung</i>")),
            (P("<b>Spital Liestal</b>"), P("<b>061 925 25 25</b>"),
             P("<b>Wo ist es geschehen?</b><br/><i>-Ortsbezeichnung/Koordinaten<br/>Siehe Situationsplan</i>")),
            (P("<b>Medbase Toujours Pratteln<br/>am Bahnhof Pratteln</b>"), P("<b>061 925 25 25</b>"), ""),
            (P("<b>Förster (Markus Eichenberger)</b>"), P("<b>079 344 65 12</b>"), ""),
        ]
        yellow = colors.Color(1, 1, 0)
        moccasin = colors.Color(0.95, 0.85, 0.65)
        orange = colors.Color(1, 0.7, 0)
        table = Table(notfall_data,
            colWidths = [7*cm, 3.2*cm, 7*cm],
            #rowHeights = [0.7*cm] * len(notfall_data),
            style=TableStyle([
                ('INNERGRID', (0, 0), (2, len(notfall_data)-1), 0.25, colors.black),
                ('BACKGROUND', (0, 0), (1, 0), yellow),
                ('BACKGROUND', (0, 1), (1, 1), moccasin),
                ('BACKGROUND', (0, 2), (1, 2), yellow),
                ('BACKGROUND', (0, 3), (1, 3), moccasin),
                ('BACKGROUND', (0, 4), (1, 4), yellow),
                ('BACKGROUND', (0, 5), (1, 5), moccasin),
                ('BACKGROUND', (1, 0), (-1, -1), orange),
                ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                ('BOX', (0, 0), (-1, -1), 1, colors.black),
                ('FONTSIZE', (0, 0), (-1, -1), 12),
                ('SPAN', (2, 3), (2, 5)),
                ('BOTTOMPADDING', (0, -1), (-1, -1), 6),
            ]),
            hAlign='RIGHT'
        )
        story.append(table)

    def get_data_table(self, data_mapping):
        # data_mapping is a list of tuples [(title, value), ...]
        data_lines = [("%s:" % line[0], Paragraph(line[1] or '', self.style_n))
                      for line in data_mapping]
        return Table(data_lines,
            colWidths = [4*cm, 4.7*cm],
            rowHeights = [None] * len(data_lines),
            style=TableStyle([
                ('INNERGRID', (0, 0), (1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (1, -1), 0.25, colors.black),
                ('VALIGN', (0, 0), (0, -1), 'TOP'),
            ]),
            hAlign='LEFT')

    def produce_pdf(self, response, context, **build_kwargs):
        assert {
            'subtitle', 'left_attrs', 'right_attrs', 'object_name', 'map_layer'
        }.issubset(set(context.keys())), "Some keys are missing in the context"
        self.doc = SimpleDocTemplate(
            response, title=str(self.instance), pagesize=A4,
            leftMargin=2*cm, rightMargin=1.5*cm, topMargin=1*cm, bottomMargin=1.5*cm
        )
        self.page_width = self.doc.width

        story = self.produce_story(context)

        self.doc.build(story, onFirstPage=self.draw_footer, **build_kwargs)
        for img in self.imgs:
            os.unlink(img.name)

    def produce_story(self, context):
        story = []

        self.produce_page1(story, context)
        if context.get('detailed_map', True):
            self.produce_detailed_map(story, context)
        if context['object_name'] == 'Unterhalt' and self.instance.belegung_set.exists():
            self.produce_unterhalt_belegung(story)
        return story

    def write_header(self, story, text=None):
        style_head = copy.deepcopy(self.styles['Heading1'])
        style_head.fontName = 'Helvetica'
        if text:
            header_data = [[
                Paragraph(text, style_head), self.header_image
            ]]
            story.append(
                Table(header_data,
                      colWidths = [self.page_width-(8*cm), 8*cm],
                      style=TableStyle([('VALIGN', (0, 0), (-1, -1), 'TOP')]))
            )
        else:
            story.append(self.header_image)

    def produce_page1(self, story, context):
        self.write_header(story, text=context['subtitle'])

        story.append(Spacer(0, 0.4*cm))
        story.extend([Paragraph(self.instance.title, self.styles['Heading1']),
                 Spacer(0, 0.3*cm)])

        self.append_notfall_table(story)
        story.append(Spacer(0, 0.5*cm))

        left_data = []
        for title, content, always_show in context['left_attrs']:
            if not always_show and not content:
                continue
            title = "<b>%s:</b><br/>" % title if title else ""
            left_data.extend([
                Paragraph("%s%s" % (title, content), self.style_n),
                Spacer(0, 0.3*cm)
            ])
        right_data = self.get_data_table(context['right_attrs'])

        story.append(
            Table([[left_data, right_data]],
                  colWidths= [self.page_width/2, self.page_width/2],
                  style=TableStyle([('VALIGN', (0, 0), (1, 0), 'TOP')]))
        )

        if self.main_map_on_first_page:
            self.produce_map(story, context)

        story.append(Spacer(0, 0.4*cm))
        if 'foot_data' in context:
            foot_data = [[Paragraph(cell, self.style_14) for cell in line] for line in context['foot_data']]
        else:
            foot_data = [[
                Paragraph("%s besichtigt und besprochen:" % context['object_name'], self.style_14),
                Paragraph("Betriebsleiter:", self.style_14)
            ]]
            if context['object_name'] == 'Schlag':
                foot_data.append(["", Paragraph("Unternehmer:", self.style_14)])
        story.append(
            Table(foot_data,
                  colWidths=[self.page_width*3/5, self.page_width*2/5],
                  rowHeights = [1*cm] * len(foot_data))
        )

        if not self.main_map_on_first_page:
            story.append(PageBreak())
            self.produce_map(story, context)

    def produce_unterhalt_belegung(self, story):
        story.append(PageBreak())
        story.append(Paragraph("Belegung", style=self.style_14))
        story.append(Spacer(0, 0.4*cm))
        bel_data = [["Schulklasse", "Wann", "Verantw. Person", "Infrastruktur", "Bemerkungen"]]
        for bel in self.instance.belegung_set.all():
            bel_data.append([
                Paragraph(bel.schulklasse or '', self.style_n), Paragraph(bel.wann, self.style_n),
                "\n".join(item for item in [bel.tel_verant_pers, bel.email_verant_pers] if item),
                Paragraph(bel.infrastruktur or '', self.style_n), Paragraph(bel.bemerkungen or '', self.style_n),
            ])
        story.append(Table(
            bel_data, colWidths=[3.5*cm] * 5, style=TableStyle([('VALIGN', (0, 0), (-1, -1), 'TOP')])
        ))

    def produce_detailed_map(self, story, context):
        story.append(PageBreak())
        try:
            im = rlImage(self.get_map_image(context['center'], context['map_layer'], 1.0, 0.4), width=17*cm, height=17*cm)
        except RuntimeError as err:
            story.append(Paragraph("Generating the map produced an error: %s" % err, self.style_n))
        else:
            story.append(im)
