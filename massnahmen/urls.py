from django.urls import path

from . import views
from .models import Pflege, Pflanzung, Planung


urlpatterns = [
    path('holznutzung/', views.HolznutzungListView.as_view(), name='holznutzung'),
    path('holznutzung/<int:pk>/detail/', views.PlanungDetailView.as_view(), name='planung_detail'),
    path('holznutzung/<int:pk>/print/', views.PlanungDetailView.as_view(output='pdf'), name='planung_pdf'),
    path('holznutzung/export/', views.ExportObjects.as_view(objClass=Planung), name='planung_export'),
    # Returns only a partial HTML, not a full page:
    path('holznutzung/analyse/', views.PlanungAnalyse.as_view(), name='planung_analyse'),

    path('pflanzung/', views.PflanzungListView.as_view(), name='pflanzung'),
    path('pflanzung/<int:pk>/detail/', views.PflanzungDetailView.as_view(), name='pflanzung_detail'),
    path('pflanzung/<int:pk>/print/', views.PflanzungDetailView.as_view(output='pdf'), name='pflanzung_pdf'),
    path('pflanzung/export/', views.ExportObjects.as_view(objClass=Pflanzung), name='pflanzung_export'),
    # Returns only a partial HTML, not a full page:
    path('pflanzung/analyse/', views.PflanzungAnalyse.as_view(), name='pflanzung_analyse'),

    path('pflege/', views.PflegeListView.as_view(), name='pflege'),
    path('pflege/<int:pk>/detail/', views.PflegeDetailView.as_view(), name='pflege_detail'),
    path('pflege/<int:pk>/print/', views.PflegeDetailView.as_view(output='pdf'), name='pflege_pdf'),
    path('pflege/export/', views.ExportObjects.as_view(objClass=Pflege), name='pflege_export'),
    # Returns only a partial HTML, not a full page:
    path('pflege/analyse/', views.PflegeAnalyse.as_view(), name='pflege_analyse'),
]
