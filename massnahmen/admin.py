from django.contrib.gis import admin

from unterhalt.forms import BaselOSMWidget
from .models import *
from .views import export_as_csv


def admin_export_as_csv(modeladmin, request, queryset):
    return export_as_csv(request, modeladmin.model._meta, queryset)
admin_export_as_csv.short_description = "Export selected objects as csv file"


admin.site.add_action(admin_export_as_csv)

@admin.register(Pflanzung)
class PflanzungAdmin(admin.GISModelAdmin):
    gis_widget = BaselOSMWidget

@admin.register(Planung)
class PlanungAdmin(admin.GISModelAdmin):
    gis_widget = BaselOSMWidget

@admin.register(Pflege)
class PflegeAdmin(admin.GISModelAdmin):
    gis_widget = BaselOSMWidget

admin.site.register(PflanzungBaumart)
admin.site.register(Baumart)
admin.site.register(MassnahmeTyp)
admin.site.register(NaturschutzMassnahme)
admin.site.register(GemeindeBL)
admin.site.register(Eigentuemer)
admin.site.register(EingriffsJahr)
