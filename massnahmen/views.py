import csv
from collections import namedtuple
from datetime import date

from django.conf import settings
from django.contrib.gis.serializers.geojson import Serializer as GeoJSONSerializer
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template.defaultfilters import slugify
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView, ListView, TemplateView

from bestand.models import Bestand, Einheit
from .models import EingriffsJahr, Planung, Pflanzung, Pflege
from .pdf_output import PlanungPDF


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'on_home': True,
            'jahre': EingriffsJahr.objects.values_list('pk', 'jahr').order_by('-jahr'),
            'pflanzung_jahre': Pflanzung.objects.exclude(realisiert=None
                ).values_list('realisiert', flat=True).distinct().order_by('-realisiert'),
            'waldrand_jahre': range(2018, date.today().year + 1),
        })
        return context


class TitleMixin:
    """ A mixin class to insert the title of the view in the context """
    def get_context_data(self, **kwargs):
        context = super(TitleMixin, self).get_context_data(**kwargs)
        context.update({'title': self.title})
        return context


class HolznutzungListView(TitleMixin, ListView):
    title = "Holznutzung"
    queryset = Planung.objects.all().order_by('-geplant')
    template_name = 'holznutzung.html'


class BaseDetailView(DetailView):
    output = 'html'
    PdfClass = None
    template_name = 'detail_base.html'
    breadcrumb = []
    edit_url_name = ''
    pdf_url_name = ''
    geom_field = 'the_geom'
    excluded_detail_fields = ('gid', 'the_geom', 'flaeche')

    def get_context_geoms(self):
        return Bestand.objects.filter(the_geom__intersects=getattr(self.object, self.geom_field)).order_by("code")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        geom = getattr(self.object, self.geom_field)
        surface = geom.area  # get area before serialization (with orig. srid)
        srid = 4326
        ser_main = GeoJSONSerializer()
        ser_main.serialize([self.object], srid=srid)
        context_geoms = self.get_context_geoms()
        ser_ctxt = GeoJSONSerializer()
        ser_ctxt.serialize(context_geoms, srid=srid)

        context.update({
            'title': self.object.title,
            'breadcrumb': self.breadcrumb,
            'map_srs': srid,
            'features': ser_main.getvalue(),
            'surface': surface,
            'center': geom.centroid,
            'fields': [(f.verbose_name, getattr(self.object, f.name), f.get_internal_type())
                        for f in self.model._meta.fields
                        if f.name not in self.excluded_detail_fields
                      ],
            'gem_pol': set([b.gemeinde for b in context_geoms if b.gemeinde not in ('', None)]),
            'context_geoms': context_geoms,
            'context_features': ser_ctxt.getvalue(),
            'change_url': reverse(self.edit_url_name, args=[self.object.pk]) if self.request.user.is_staff else '',
            'pdf_url': reverse(self.pdf_url_name, args=[self.object.pk]) if self.pdf_url_name else '',
        })
        return context

    def pdf_context(self, context):
        return context

    def render_to_response(self, context, **kwargs):
        if self.output == 'pdf':
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % slugify(str(self.object))
            self.PdfClass(self.object).produce_pdf(response, self.pdf_context(context))
            return response
        else:
            return super().render_to_response(context, **kwargs)


class PlanungDetailView(BaseDetailView):
    model = Planung
    #template_name = 'planung_detail.html'
    PdfClass = PlanungPDF
    breadcrumb = [("Holznutzung", reverse_lazy('holznutzung'))]
    edit_url_name = 'admin:massnahmen_planung_change'
    pdf_url_name = 'planung_pdf'

    def pdf_context(self, context):
        context.update({
            'subtitle': "Schlagplanung",
            'object_name': "Schlag",
            'map_layer': "planung",
            'left_attrs': [("Arbeitsauftrag (Ziel)", self.object.arbeitsauftrag or '-', True),
                           ("Schlagchef", self.object.schlagchef or '-', True),
                           ("Bemerkungen", self.object.bemerkunge, False)],
            'right_attrs': [("Lokalname", self.object.lokalname),
                            ("Massnahme", str(self.object.typ)),
                            ("Fläche", "%.2f m<sup>2</sup>" % context['surface']),
                            ("Schlagvorbereitung", pretty_bool(self.object.schlagvorbereitung)),
                            ("Zwangnutzung", pretty_bool(self.object.zwangsnutzung)),
                            ("Holzvolumen (sv)", str(self.object.geschaetzt))],
        })
        return context


LineTuple = namedtuple('LineTuple',
    ['pk', 'eigentumer_id', 'eigentumer', 'massnahme_id', 'massnahme', 'jahr', 'flaeche', 'nach_verj']
)

class PlanungAnalyse(TemplateView):
    template_name = 'planung_analyse.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jahr = get_object_or_404(EingriffsJahr, pk=self.request.GET.get('jahr'))
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM "Analyse: Nutzungsplanung" WHERE jahr=%s', [jahr.jahr])
            context['lines'] = []
            for row in cursor.fetchall():
                context['lines'].append(LineTuple(*row))
        return context


class PflanzungListView(TitleMixin, ListView):
    title = "Pflanzungen"
    queryset = Pflanzung.objects.all().order_by('-realisiert')
    template_name = 'pflanzungen.html'


class PflanzungDetailView(BaseDetailView):
    model = Pflanzung
    PdfClass = PlanungPDF
    pdf_url_name = 'pflanzung_pdf'
    breadcrumb = [("Pflanzungen", reverse_lazy('pflanzung'))]
    edit_url_name = 'admin:massnahmen_pflanzung_change'
    template_name = 'pflanzung_detail.html'
    geom_field = 'geom'
    excluded_detail_fields = ('id', 'geom', 'flaeche', 'baumarten_list')

    def get_context_geoms(self):
        return Einheit.objects.filter(the_geom__intersects=self.object.geom).order_by("geom_id")

    def pdf_context(self, context):
        context.update({
            'subtitle': "Pflanzung",
            'object_name': "Pflanzung",
            'map_layer': "pflanzung",
            'left_attrs': [("Verantwortliche Person", '', True),
                           ("Bemerkungen", self.object.bemerkungen, False)],
            'right_attrs': [("Lokalname", self.object.lokalname),
                            ("Zaunlänge", str(self.object.zaunlaenge)),
                            ("Baumarten", ", ".join([str(ba) for ba in self.object.baumarten_list.all()]) or '-'),
                           ]
        })
        return context


class PflanzungAnalyse(TemplateView):
    template_name = 'pflanzung_analyse.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jahr = int(self.request.GET.get('jahr'))
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT * FROM "Analyse: Pflanzung" WHERE pflanzungsdatum_realisiert=%s ORDER BY eigentuemer',
                [jahr]
            )
            context['lines'] = []
            LineTuple = namedtuple('LineTuple',
                ['jahr', 'eigentumer', 'baumart', 'anzahl', 'flaechen']
            )
            for row in cursor.fetchall():
                context['lines'].append(LineTuple(*row))
        return context


class PflegeListView(TitleMixin, ListView):
    title = "Jungwaldpflege"
    queryset = Pflege.objects.all().order_by('-geplant__jahr')
    template_name = 'pflegen.html'


class PflegeDetailView(BaseDetailView):
    model = Pflege
    PdfClass = PlanungPDF
    pdf_url_name = 'pflege_pdf'
    breadcrumb = [("Jungwaldpflege", reverse_lazy('pflege'))]
    edit_url_name = 'admin:massnahmen_pflege_change'
    excluded_detail_fields = BaseDetailView.excluded_detail_fields + ('eingriffsjahr',)

    def pdf_context(self, context):
        context.update({
            'subtitle': "Jungwaldpflege",
            'object_name': "Pflege",
            'map_layer': "pflege",
            'left_attrs': [("Arbeitsauftrag (Ziel)", self.object.pflegeziel or '-', True),
                           ("Bestockungsziel", self.object.bestockungsziel or '-', True),
                           ("Verantwortliche Person", self.object.ver_person or '-', True),
                           ("Bemerkungen", self.object.bemerkungen or '-', False),
                          ],
            'right_attrs': [("Lokalname", self.object.lokalname),
                            ("Pflegetyp", str(self.object.pflegetyp)),
                            ("Fläche", "%.2f m<sup>2</sup>" % context['surface']),
                            ("Neigung >50%", pretty_bool(self.object.neigung)),
                            ("Wertastung", pretty_bool(self.object.wertastung)),
                            ("Nächster Eingriff", str(self.object.naechster_eingriff) if self.object.naechster_eingriff else '-'),
                           ],
        })
        return context


class PflegeAnalyse(TemplateView):
    template_name = 'pflege_analyse.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jahr_pk = int(self.request.GET.get('jahr'))
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT * FROM "Analyse: Pflegeflaechen" WHERE jahr_id=%s ORDER BY eigentuemer',
                [jahr_pk]
            )
            context['lines'] = []
            LineTuple = namedtuple('LineTuple',
                ['jahr', 'eigentumer', 'entwicklungsstufe', 'flaeche', 'jahr_id']
            )
            for row in cursor.fetchall():
                context['lines'].append(LineTuple(*row))
        return context


class ExportObjects(ListView):
    objClass = None
    def get_queryset(self):
        ids = [int(i) for i in self.request.GET['ids'].split(',')]
        return self.objClass.objects.filter(pk__in=ids)

    def render_to_response(self, context, **kwargs):
        return export_as_csv(self.request, self.objClass._meta, context['object_list'])


def export_as_csv(request, opts, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s.csv' % str(opts).replace('.', '_')
    writer = csv.writer(response)
    field_names = [field.name for field in opts.fields if field.name!='the_geom']
    # Write headers
    writer.writerow(field_names)
    # Write data rows
    for obj in queryset:
        writer.writerow([getattr(obj, field) or '' for field in field_names if field!='the_geom'])
    return response

def pretty_bool(val):
    return "Ja" if val else "Nein"
