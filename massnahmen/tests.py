from django.contrib.auth.models import User
from django.contrib.gis.geos import Polygon
from django.test import TestCase
from django.urls import reverse

from .models import Eigentuemer, EingriffsJahr, Pflege, PflegeTyp


class MassnahmenTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username='user', password='pwd')

    def test_fplege_pdf_export(self):
        pflege = Pflege.objects.create(
            lokalname='Pflege Test',
            geplant=EingriffsJahr.objects.create(id=1, jahr=2024),
            pflegetyp=PflegeTyp.objects.create(id=1, pflegetyp='Normal'),
            the_geom=Polygon(((0.0, 0.0), (0.0, 50.0), (50.0, 50.0), (50.0, 0.0), (0.0, 0.0))),
            eigentum=Eigentuemer.objects.create(id=1, name='Muttenz'),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('pflege_pdf', args=[pflege.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')
