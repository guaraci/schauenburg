from functools import partial

from massnahmen.pdf_output import PlanungPDF

from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.platypus import PageBreak, Paragraph, Spacer, Table, TableStyle


class WaldrandPDF(PlanungPDF):
    def produce_pdf(self, response, context, **build_kwargs):
        return super().produce_pdf(response, context, onLaterPages=self.draw_footer)

    def draw_footer(self, canvas, doc):
        super().draw_footer(canvas, doc)
        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        canvas.drawRightString(canvas._pagesize[0] - 1.6 * cm, 1.4 * cm, str(canvas._pageNumber))
        canvas.restoreState()

    def produce_story(self, context):
        story = []
        P = partial(Paragraph, style=self.style_n)

        self.write_header(story, text=context['subtitle'])
        story.append(Spacer(0, 0.4*cm))
        story.extend([
            Spacer(0, 0.4*cm),
            Paragraph(self.instance.title, self.styles['Heading1']),
            Spacer(0, 0.3*cm)
        ])
        self.append_notfall_table(story)
        story.extend([
            Spacer(0, 0.2*cm),
            Paragraph("<b><u>Pflegeziel:</u></b>", self.style_11),
            P(self.instance.pflegeziel or "-"),
            Spacer(0, 0.2*cm),
            Paragraph(f"<b>Verantwortlich:</b> - {self.instance.ver_person}", self.style_n),
            Spacer(0, 0.2*cm),
            Paragraph("<b><u>Bemerkungen:</u></b>", self.style_11),
            P(self.instance.bemerkungen or '-'),
            Spacer(0, 0.2*cm),
            Paragraph("<b><u>Situationsplan:</u></b>", self.style_11),
        ])
        self.produce_map(story, context)

        story.append(PageBreak())
        story.append(self.header_image)
        story.append(Spacer(0, 0.4*cm))
        story.append(Paragraph("Arbeitsrapport", style=self.style_14))
        story.append(Spacer(0, 0.2*cm))
        obj = context['object']
        data = [
            [P('<b>Auftrag:</b>'), P(f'<b>{obj.title}</b>')],
            [P('<b>Rapportierung:</b>'), P(f'<b>{obj.rapportierung}</b>')],
            [P('<b>Arbeitsort:</b>'), P(f'<b>{obj.eigentum}</b>')],
        ]
        story.append(Table(
            data, colWidths=[4*cm, 13*cm] * 5, style=TableStyle([('VALIGN', (0, 0), (-1, -1), 'TOP')])
        ))
        story.append(Spacer(0, 0.3*cm))
        data = [
            [Paragraph('<i>Datum</i>', self.style_n), Paragraph('<i>Arbeitsbeschrieb</i>', self.style_n)]
        ]+ 20 * [[''] * 11]
        story.append(Table(
            data, colWidths=[1.5*cm, 3.7*cm] + [1.3*cm] * 9, style=TableStyle([
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ])
        ))
        story.append(Spacer(0, 0.3*cm))
        story.append(P(" <b>Maschinen/Benzin</b>"))
        story.append(Table(
            [['Raupenseilwinde', '', ''], ['Forwarder', '', ''], ['Motorsäge/Freischneider', '', ''],
             ['Bagger', '', ''], ['', '', ''], ['', '', '']],
            colWidths=[5.2*cm, 10.5*cm, 1.3*cm],
            style=TableStyle([
                ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ])
        ))
        return story
