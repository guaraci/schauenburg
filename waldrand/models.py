from django.db import models
from django.contrib.gis.db import models as gis_models
from django.urls import reverse

from massnahmen.models import Eigentuemer


class Waldrand(models.Model):
    EINGRIFF_CHOICES = (
        (1, "Ersteingriff"),
        (2, "Folgeeingriff"),
        (3, "Erst- und Folgeeingriff"),
    )
    name = models.CharField("Name", max_length=200)
    objekt_nr = models.IntegerField("Objekt Nr.", blank=True, null=True)
    lauf_nr = models.IntegerField("Lauf Nr.", blank=True, null=True)
    eingriffsjahr = models.CharField(max_length=10, blank=True,
        db_column="planung_eingriffsjahr", help_text="Geplantes Eingriffsjahr")
    pflegeziel = models.TextField(blank=True)
    bestockungsziel = models.TextField(blank=True)
    rapportierung = models.TextField("Rapportierung", blank=True)
    bemerkungen = models.TextField("Bemerkungen, Besonderheiten", blank=True)
    eingriffstyp = models.SmallIntegerField(blank=True, null=True, choices=EINGRIFF_CHOICES)
    naechster_eingriff = models.IntegerField("Nächster Eingriff", null=True, blank=True,
        help_text="Nächster Eingriff vorgesehen im Jahr 20?? (z.b. \"2020\")")
    eigentum = models.ForeignKey(Eigentuemer, null=True, blank=True, on_delete=models.SET_NULL)
    ver_person = models.CharField("Verantwortliche Person", max_length=150, blank=True)
    line = gis_models.LineStringField("Standort", srid=2056)
    tiefe = models.IntegerField("Waldrandtiefe", null=True, blank=True, default=15,
        help_text="Durchschnittliche Waldrandtiefe in Meter")

    class Meta:
        db_table = 'waldrand'

    def __str__(self):
        return self.name

    @property
    def title(self):
        return "%s (%s); %s" % (self.name, self.eingriffsjahr, self.eigentum)

    def get_absolute_url(self):
        return reverse('waldrand-detail', args=[self.pk])


class WaldrandPflegeKonzept(models.Model):
    WALDRANDTY_CHOICES = (
        (1, 'kantonaler Waldrand Priorität 1'),
        (2, 'kantonaler Waldrand Priorität 2'),
        (3, 'kantonaler Waldrand Priorität 3'),
        (4, 'kommunaler Waldrand Priorität 1'),
        (5, 'kommunaler Waldrand Priorität 2'),
        (6, 'kommunaler Waldrand Priorität 3'),
    )
    gid = models.IntegerField(primary_key=True)
    waldrandty = models.IntegerField(choices=WALDRANDTY_CHOICES)
    nr = models.IntegerField(null=True)
    eigentum = models.ForeignKey(Eigentuemer, null=True, blank=True, db_column='eigentum', on_delete=models.SET_NULL)
    eingriffstyp = models.SmallIntegerField(blank=True, null=True)
    kosten = models.FloatField()
    geom = gis_models.MultiLineStringField(srid=2056)

    class Meta:
        db_table = 'waldrandpflegekonzept_lv95'
        managed=False

    def __str__(self):
        return f"Konzept Nr. {self.nr}"
