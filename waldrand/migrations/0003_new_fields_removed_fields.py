from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldrand', '0002_waldrandpflegekonzept'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='waldrand',
            name='ansatz_pflegeeingriff',
        ),
        migrations.RemoveField(
            model_name='waldrand',
            name='auszahlungsbetrag',
        ),
        migrations.RemoveField(
            model_name='waldrand',
            name='baumart',
        ),
        migrations.RemoveField(
            model_name='waldrand',
            name='neigung',
        ),
        migrations.AddField(
            model_name='waldrand',
            name='lauf_nr',
            field=models.IntegerField(blank=True, null=True, verbose_name='Lauf Nr.'),
        ),
        migrations.AddField(
            model_name='waldrand',
            name='objekt_nr',
            field=models.IntegerField(blank=True, null=True, verbose_name='Objekt Nr.'),
        ),
        migrations.AddField(
            model_name='waldrand',
            name='rapportierung',
            field=models.TextField(blank=True, verbose_name='Rapportierung'),
        ),
        migrations.AlterField(
            model_name='waldrand',
            name='tiefe',
            field=models.IntegerField(blank=True, default=15, help_text='Durchschnittliche Waldrandtiefe in Meter', null=True, verbose_name='Waldrandtiefe'),
        ),
    ]
