from django.contrib.gis.db.models import fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('massnahmen', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Waldrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Name')),
                ('eingriffsjahr', models.CharField(blank=True, db_column='planung_eingriffsjahr', help_text='Geplantes Eingriffsjahr', max_length=10)),
                ('pflegeziel', models.TextField(blank=True)),
                ('bestockungsziel', models.TextField(blank=True)),
                ('bemerkungen', models.TextField(blank=True, verbose_name='Bemerkungen, Besonderheiten')),
                ('auszahlungsbetrag', models.IntegerField(blank=True, null=True)),
                ('ansatz_pflegeeingriff', models.IntegerField(blank=True, null=True, verbose_name='Ansatz Pflegeeingriff')),
                ('naechster_eingriff', models.IntegerField(blank=True, help_text='Nächster Eingriff vorgesehen im Jahr 20?? (z.b. "2020")', null=True, verbose_name='Nächster Eingriff')),
                ('neigung', models.BooleanField(default=False, help_text='angekreuzt: Neigung >50%')),
                ('baumart', models.BooleanField(default=False, help_text='angekreuzt: Fi, Ta, Dgl, Lä')),
                ('ver_person', models.CharField(blank=True, max_length=150, verbose_name='Verantwortliche Person')),
                ('line', fields.LineStringField(srid=2056, verbose_name='Standort')),
                ('tiefe', models.IntegerField(blank=True, help_text='Durchschnittliche Waldrandtiefe in Meter', null=True, verbose_name='Waldrandtiefe')),
                ('eigentum', models.ForeignKey(blank=True, null=True, on_delete=models.SET_NULL, to='massnahmen.Eigentuemer')),
            ],
            options={
                'db_table': 'waldrand',
            },
        ),
    ]
