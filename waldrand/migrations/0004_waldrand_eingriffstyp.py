from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('waldrand', '0003_new_fields_removed_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='waldrand',
            name='eingriffstyp',
            field=models.SmallIntegerField(blank=True, choices=[(1, 'Ersteingriff'), (2, 'Folgeeingriff'), (3, 'Erst- und Folgeeingriff')], null=True),
        ),
    ]
