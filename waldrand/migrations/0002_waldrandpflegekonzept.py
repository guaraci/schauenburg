import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('massnahmen', '0008_booleans_nullable'),
        ('waldrand', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaldrandPflegeKonzept',
            fields=[
                ('gid', models.IntegerField(primary_key=True, serialize=False)),
                ('waldrandty', models.IntegerField()),
                ('nr', models.IntegerField(null=True)),
                ('eingriffstyp', models.SmallIntegerField(blank=True, null=True)),
                ('kosten', models.FloatField()),
                ('geom', django.contrib.gis.db.models.fields.MultiLineStringField(srid=2056)),
                ('eigentum', models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='massnahmen.eigentuemer')),
            ],
            options={
                'db_table': 'waldrandpflegekonzept_lv95',
                'managed': False,
            },
        ),
    ]
