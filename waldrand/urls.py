from django.urls import path

from. import views

urlpatterns = [
    path('', views.WaldrandListView.as_view(), name='waldrand-list'),
    path('<int:pk>/', views.WaldrandDetailView.as_view(), name='waldrand-detail'),
    path('<int:pk>/pdf/', views.WaldrandPDFView.as_view(), name='waldrand-pdf'),
    # Returns only a partial HTML, not a full page:
    path('analyse/', views.WaldrandAnalyse.as_view(), name='waldrand_analyse'),
]
