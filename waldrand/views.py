from collections import namedtuple

from django.db import connection
from django.http import HttpResponse
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.views.generic import DetailView, ListView, TemplateView

from massnahmen.utils import GeoJSONSerializer
from massnahmen.views import TitleMixin

from .models import Waldrand
from .pdf_output import WaldrandPDF


class WaldrandListView(TitleMixin, ListView):
    title = "Waldrandpflege"
    queryset = Waldrand.objects.all().order_by('-eingriffsjahr')
    template_name = 'waldrand/list.html'


class WaldrandDetailView(DetailView):
    model = Waldrand
    template_name = 'waldrand/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        s = GeoJSONSerializer()
        s.serialize([self.object], fields=('pk', 'name', 'line'), srid=4326)
        context.update({
            'breadcrumb': [("Waldrand", reverse('waldrand-list'))],
            'title': self.object.title,
            'fields': [(f.verbose_name, getattr(self.object, f.name), f.get_internal_type())
                        for f in self.model._meta.fields
                        if f.name not in ('id', 'line')
                      ],
            'features': s.getvalue(),
            'change_url': (
                reverse('admin:waldrand_waldrand_change', args=[self.object.pk])
                if self.request.user.is_staff else ''
            ),
            'pdf_url': reverse('waldrand-pdf', args=[self.object.pk]),
        })
        return context


class WaldrandPDFView(WaldrandDetailView):
    def pdf_context(self, context):
        def pretty_bool(val):
            return "Ja" if val else "Nein"

        context.update({
            'subtitle': "Waldrandplanung",
            'object_name': "Waldrand",
            'map_layer': "waldrand",
            'detailed_map': False,
            'center': self.object.line.centroid,
            'left_attrs': [
                ("Bestockungsziel", self.object.bestockungsziel or '-', True),
                ("Verantwortliche Person", self.object.ver_person or '-', True),
                ("Bemerkungen", self.object.bemerkungen or '-', True)
            ],
            'right_attrs': [
                ("Lokalname", self.object.name),
                ("Nächster Eingriff", str(self.object.naechster_eingriff or '-')),
            ],
        })
        return context

    def render_to_response(self, context, **kwargs):
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % slugify(str(self.object))
        WaldrandPDF(self.object).produce_pdf(response, self.pdf_context(context))
        return response


class WaldrandAnalyse(TemplateView):
    template_name = 'waldrand/waldrand_analyse.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jahr = self.request.GET.get('jahr')
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT * FROM bv_analyse_waldrandlaenge_pro_eigentuemer_und_jahr bv
                WHERE planung_eingriffsjahr=%s ORDER BY eigentuemer''', [jahr]
            )
            context['lines'] = []
            LineTuple = namedtuple('LineTuple',
                ['jahr', 'eigentumer_id', 'eigentumer', 'lange_in_m']
            )
            for row in cursor.fetchall():
                context['lines'].append(LineTuple(*row))
        return context


class WaldrandMapView(TemplateView):
    # FIXME: Not yet functional
    template_name = 'waldrand/map.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        s = GeoJSONSerializer()
        s.serialize(Waldrand.objects.all(), fields=('pk', 'name', 'line'), srid=2056)
        context.update({
            'features': s.getvalue(),
        })
        return context
