from django.contrib.gis import admin

from .models import Waldrand, WaldrandPflegeKonzept


@admin.register(Waldrand)
class WaldrandAdmin(admin.GISModelAdmin):
    list_display = ['name', 'objekt_nr', 'eingriffsjahr', 'eigentum', 'ver_person']


@admin.register(WaldrandPflegeKonzept)
class WaldrandPflegeKonzeptAdmin(admin.GISModelAdmin):
    list_display = ['nr', 'waldrandty', 'eigentum', 'kosten']
