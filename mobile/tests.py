import time
from contextlib import contextmanager
from datetime import date, timedelta
from pathlib import Path
from unittest import skipUnless

from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.files import File
from django.test import TestCase, modify_settings, override_settings
from django.urls import reverse
from django.utils import timezone

from selenium.webdriver import Firefox
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from massnahmen.models import Eigentuemer
from unterhalt.models import (
    Eigentuemertyp, Einrichtungsart, Freizeiteinrichtung, Kontaktperson, Unterhalt,
)

try:
    import NetworkManager
    HAS_NETWORK_MANAGER = True
except ImportError:
    # python-networkmanager not available
    HAS_NETWORK_MANAGER = False


@contextmanager
def disable_connection(driver):
    """
    If we find a way to toggle Chrome Offline mode, we should replace this
    function by that capability.
    """
    if isinstance(driver, ChromeDriver):
        driver.set_network_conditions(
            offline=True, latency=5, download_throughput=500 * 1024, upload_throughput=500 * 1024
        )
        try:
            yield
        finally:
            driver.set_network_conditions(
                offline=False, latency=5, download_throughput=500 * 1024, upload_throughput=500 * 1024
            )
        time.sleep(0.5)
        return

    # With Firefox, this does not always work. Some versions always remains "online"
    # as long as the manual Offline mode is selected :-(
    def get_active_device():
        for dev in NetworkManager.NetworkManager.GetDevices():
            if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                return dev

    dev = get_active_device()
    conn = get_active_device().ActiveConnection.Connection
    dev.Disconnect()
    time.sleep(0.5)
    try:
        yield
    finally:
        NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
    time.sleep(0.5)

class ProjectDataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='user', password='secret')
        freiz = Freizeiteinrichtung.objects.create(
            name='Meine Name',
            beschreibung = 'Eine beschreibung',
            obj_art=Einrichtungsart.objects.create(name='Bänkli'),
            werkeigentuemer=Eigentuemertyp.objects.create(name='Verkehrs- und Verschönerungsverein'),
            waldbesitzer=Eigentuemer.objects.create(id=1, name='BG Muttenz'),
            kontaktperson=Kontaktperson.objects.create(name='Hans Schmid'),
            the_geom=Point(2616291, 1263165, srid=2056),
        )
        Unterhalt.objects.create(
            objekt=freiz,
            datum=timezone.now() - timedelta(days=180),
            massnahme_objekt="Grill erneuern.",
            umsetzung=2,
        )


# Like common.mobile_settings.py
@override_settings(ROOT_URLCONF='mobile.urls', LOGIN_REDIRECT_URL='/')
@modify_settings(INSTALLED_APPS={'append': 'mobile'})
class MobileTests(ProjectDataMixin, TestCase):
    def test_login(self):
        response = self.client.get(reverse('home'), follow=True)
        self.assertContains(
            response,
            '<input type="password" name="password" autocomplete="current-password" required id="id_password">',
            html=True
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertContains(response, '<div id="infos-div" class="hidden">')

    def test_getting_inventory(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('inventory')).json()
        einrichtung = Freizeiteinrichtung.objects.first()
        self.assertEqual(response['type'], 'FeatureCollection')
        self.assertEqual(response['crs']['properties']['name'], 'EPSG:4326')
        self.assertEqual(response['features'][0]['properties']['name'], 'Meine Name')
        self.assertEqual(response['features'][0]['properties']['obj_art'], [einrichtung.obj_art_id, 'Bänkli'])
        self.assertEqual(response['features'][0]['geometry']['type'], 'Point')
        self.assertTrue(7.6 <= response['features'][0]['geometry']['coordinates'][0] <= 7.7)
        self.assertEqual(
            response['features'][0]['properties']['unterhalt_set'][0]['massnahme_objekt'],
            'Grill erneuern.'
        )

    def test_save_einrichtung(self):
        einrichtung = Freizeiteinrichtung.objects.first()
        self.client.force_login(self.user)
        response = self.client.post(reverse('save-einrichtung'), data={
            'id': einrichtung.pk,
            'name': 'Meine Name', 'beschreibung': 'Eine beschreibung', 'obj_art': einrichtung.obj_art.pk,
            'werkeigentuemer': einrichtung.werkeigentuemer.pk, 'waldbesitzer': einrichtung.waldbesitzer.pk,
            'kontaktperson': einrichtung.kontaktperson.pk, 'unterhaltsmassnahmen': '',
            'bemerkung_unterhalt': '',
            'new_lon': ['7.661612031006465'], 'new_lat': ['47.51815715076843'],
        })
        self.assertEqual(response.json()['result'], 'OK')
        einrichtung.refresh_from_db()
        self.assertAlmostEqual(einrichtung.the_geom.x, 2616796, places=0)
        self.assertAlmostEqual(einrichtung.the_geom.y, 1263070, places=0)

    def test_save_massnahme(self):
        img_path = Path(__file__).parent / 'static' / 'img' / 'plan.png'
        einrichtung = Freizeiteinrichtung.objects.first()
        self.client.force_login(self.user)
        with img_path.open('rb') as fh:
            response = self.client.post(reverse('save-massnahme'), data={
                'objekt':  einrichtung.pk,
                'datum': f'{date.today().year}-05-04',
                'massnahme_objekt': 'Some text',
                'unterhalt_objekt': 'on',
                'massnahmen_baumbestand': '',
                'erledigt': '',
                'foto': File(fh),
            })
        self.assertEqual(response.json()['result'], 'OK')
        massnahme = einrichtung.unterhalt_set.get(massnahme_objekt='Some text')
        self.assertTrue(massnahme.foto)
        self.assertIs(massnahme.unterhalt_objekt, True)


@override_settings(ROOT_URLCONF='mobile.urls', LOGIN_REDIRECT_URL='/')
@modify_settings(INSTALLED_APPS={'append': 'mobile'})
class MobileSeleniumTests(ProjectDataMixin, StaticLiveServerTestCase):
    _overridden_settings = {'DEBUG': True}  # To obtain any traceback in the terminal!

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        #driver = 'chrome'
        driver = 'firefox'
        if driver == 'chrome':
            cls.selenium = ChromeDriver()
        elif driver == 'firefox':
            prf = Options()
            prf.set_preference("geo.prompt.testing", True)
            prf.set_preference("geo.prompt.testing.allow", True)
            cls.selenium = Firefox(options=prf)
        cls.selenium.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def setUp(self):
        super().setUp()
        self.setUpTestData()

    def login(self):
        self.selenium.get('%s/' % self.live_server_url)
        username_input = self.selenium.find_element(By.NAME, "username")
        username_input.send_keys('user')
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('secret')
        self.selenium.find_element(By.XPATH, "//button[@type='submit']").click()

    def go_to_einrichtung(self):
        for marker in self.selenium.find_elements(By.CLASS_NAME, 'leaflet-marker-icon'):
            classes = marker.get_attribute('class').split()
            if 'icon-plot-red' in classes or 'icon-plot-orange' in classes:
                break
        else:
            self.fail("No marker with icon-plot-red/icon-plot-orange classes")
        marker.click()


class MobileOnlineSeleniumTests(MobileSeleniumTests):
    def test_edit_einrichtung_online(self):
        new_pers = Kontaktperson.objects.create(name='John Doe')
        self.login()
        self.go_to_einrichtung()
        # Edit Unterhalt
        self.selenium.find_element(By.ID, "edit-plot").click()
        Select(
            self.selenium.find_element(By.ID, "id_kontaktperson")
        ).select_by_visible_text("John Doe")
        self.selenium.find_element(By.ID, "save-plot").click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, "edit-plot"))
        )
        freiz = Freizeiteinrichtung.objects.get(name='Meine Name')
        self.assertEqual(freiz.kontaktperson, new_pers)
        # Add Massnahme
        self.selenium.find_element(By.ID, "new-massnahme").click()
        self.selenium.find_element(By.ID, "id_massnahme_objekt").send_keys('Bla bla')
        self.selenium.find_element(By.ID, "save-mass").click()
        ul = WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, "mass-list"))
        )
        self.assertEqual(len(ul.find_elements(By.TAG_NAME, 'li')), 2)
        massnahme = freiz.unterhalt_set.get(massnahme_objekt='Bla bla')
        self.assertEqual(massnahme.unterhalt_objekt, True)
        self.assertEqual(massnahme.datum.date(), date.today())
        # Edit Massnahme
        ul.find_elements(By.TAG_NAME, 'li')[0].click()
        self.selenium.find_element(By.ID, "id_massnahme_objekt").clear()
        self.selenium.find_element(By.ID, "id_massnahme_objekt").send_keys('Ho ho!')
        self.selenium.find_element(By.ID, "save-mass").click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, "mass-list"))
        )
        massnahme.refresh_from_db()
        self.assertEqual(massnahme.massnahme_objekt, 'Ho ho!')
        self.assertIn('Ho ho!', ul.find_elements(By.TAG_NAME, 'li')[0].text)


class MobileOfflineSeleniumTests(MobileSeleniumTests):
    @skipUnless(HAS_NETWORK_MANAGER, 'python-networkmanager is needed to connect/disconnect the network')
    def test_edit_einrichtung_offline(self):
        new_pers = Kontaktperson.objects.create(name='John Doe')
        self.login()
        self.go_to_einrichtung()
        with disable_connection(self.selenium):
            self.selenium.get('%s/' % self.live_server_url)
            self.go_to_einrichtung()
            # Edit Unterhalt
            self.selenium.find_element(By.ID, "edit-plot").click()
            Select(
                self.selenium.find_element(By.ID, "id_kontaktperson")
            ).select_by_visible_text("John Doe")
            self.selenium.find_element(By.ID, "save-plot").click()
            self.assertEqual(self.selenium.find_element(By.ID, "unsync-counter").text, '1')
            # Add Massnahme
            self.selenium.find_element(By.ID, "new-massnahme").click()
            self.selenium.find_element(By.ID, "id_massnahme_objekt").send_keys('Bla bla')
            self.selenium.find_element(By.ID, "save-mass").click()
            self.assertEqual(self.selenium.find_element(By.ID, "unsync-counter").text, '2')
            # Edit new Massnahme
            ul = WebDriverWait(self.selenium, 4).until(
                EC.visibility_of_element_located((By.ID, "mass-list"))
            )
            ul.find_elements(By.TAG_NAME, 'li')[0].click()
            self.selenium.find_element(By.ID, "id_massnahme_objekt").clear()
            self.selenium.find_element(By.ID, "id_massnahme_objekt").send_keys('Ho ho!')
            self.selenium.find_element(By.ID, "save-mass").click()
            ul = WebDriverWait(self.selenium, 4).until(
                EC.visibility_of_element_located((By.ID, "mass-list"))
            )
            self.assertEqual(self.selenium.find_element(By.ID, "unsync-counter").text, '2')
            # Edit existing Massnahme
            ul.find_elements(By.TAG_NAME, 'li')[1].click()
            self.selenium.find_element(By.ID, "id_massnahmen_baumbestand").send_keys('Bestand')
            self.selenium.find_element(By.ID, "save-mass").click()
            ul = WebDriverWait(self.selenium, 4).until(
                EC.visibility_of_element_located((By.ID, "mass-list"))
            )
            self.assertEqual(self.selenium.find_element(By.ID, "unsync-counter").text, '3')
        # Resync data, one alert to ask for sync confirmation, one alert for sync success.
        WebDriverWait(self.selenium, 4).until(EC.alert_is_present())
        confirm = self.selenium.switch_to.alert
        self.assertEqual(confirm.text, 'Möchten sie jetzt die neue offline Daten synchronisieren?');
        confirm.accept()
        modal = WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, "modal-content"))
        )
        self.assertEqual(modal.text, 'Die Daten sind jetzt auf dem Server gespeichert.')
        self.selenium.find_element(By.ID, "modal-ok").click()
        self.assertEqual(self.selenium.find_element(By.ID, "unsync-counter").text, '0')
        # Test results
        freiz = Freizeiteinrichtung.objects.get(name='Meine Name')
        self.assertEqual(freiz.kontaktperson, new_pers)
        massnahme = freiz.unterhalt_set.get(massnahme_objekt='Ho ho!')
        self.assertEqual(massnahme.datum.date(), date.today())
        massnahme = freiz.unterhalt_set.get(massnahme_objekt='Grill erneuern.')
        self.assertEqual(massnahme.massnahmen_baumbestand, 'Bestand')
