'use strict';
var AppVersion = "0.3.2";

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function isOnline() {
    return window.navigator.onLine;
}

function isArray(variable) {
    return Object.prototype.toString.call(variable) === '[object Array]';
}

function hide(element_or_selector) {
    const el = typeof element_or_selector === 'string' ? document.querySelector(element_or_selector) : element_or_selector;
    el.classList.add('hidden');
}

function show(element_or_selector) {
    const el = typeof element_or_selector === 'string' ? document.querySelector(element_or_selector) : element_or_selector;
    el.classList.remove('hidden');
}

function toISODate(dt) {
    if (dt) return dt.getFullYear() + '-' + (dt.getMonth() + 1).toString().padStart(2, '0') + '-' + dt.getDate().toString().padStart(2, '0');
    return dt;
}

function formToObject(form) {
    let obj = {};
    Array.from(form.querySelectorAll('input, select, textarea'))
        .filter(element => element.name)
        .forEach(element => {
            if (element.type === 'checkbox') obj[element.name] = element.checked;
            else if (element.type === 'select') obj[element.name] = [
                element.value, element.options[element.selectedIndex].text
            ];
            else obj[element.name] = element.value;
        });
    return obj;
}

function objToFormData(obj) {
    var formData = new FormData();
    for (const key in obj) {
        // Only send the value of select option pairs
        formData.append(key, isArray(obj[key]) ? obj[key][0] : obj[key]);
    }
    return formData;
}
/*
 * Use the data-toggleimg img attribute to toggle its src image.
 * `img`: either the element itself or an ID.
 */
function toggleImg(img) {
    if (!(img instanceof HTMLElement)) {
        img = document.getElementById(img);
    }
    var newSrc = img.dataset.toggleimg;
    img.dataset.toggleimg = img.src;
    img.src = newSrc;
}

function totalHeight(elemId) {
    var style = document.defaultView.getComputedStyle(document.getElementById(elemId), '');
    var result = parseInt(style.getPropertyValue('height'), 10) + parseInt(style.getPropertyValue('margin-top'), 10) + parseInt(style.getPropertyValue('margin-bottom'), 10);
    if (isNaN(result)) return 0;
    return result;
}

function loadJSON(path) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", path);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                if (xhr.responseText.includes('login-form')) {
                    // User needs to login again
                    location.reload(true);
                }
                try {
                    var content = JSON.parse(xhr.responseText);
                    resolve(content);
                } catch(e) {
                    reject({
                        status: this.status,
                        statusText: e
                    });
                }
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

/*
 * Global variables
 *  - verbose_names is defined in the index.html template
 */
var db = new PouchDB('schauenburg', {adapter: 'idb'}),
    CACHE_NAME = 'schauenburg-v1',
    currentPlot = null,
    thisYear = new Date().getFullYear(),
    einForm = null,
    massForm = null,
    unsyncCounter = null,
    progressBar = null,
    CENTER_INITIAL = [47.515, 7.67],
    ZOOM_INITIAL = 13,
    ZOOM_PLOT = 27,
    DISTANCE_WARNING_METERS = 50;

var epsg2056Converter = proj4(
    proj4('EPSG:4326'),
    "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs"
);

/*
 * Map class
 */
var Map = function () {
    this.map = L.map('map-div');//, {crs: L.TileLayer.Swiss.EPSG_2056, maxBounds: L.TileLayer.Swiss.latLngBounds});
    this.vLayers = {};
    this.centerOptions = {
        radius: 3,
        fillColor: "#ff1100"
    };
    this.map.createPane('highlightPane');
    this.map.getPane('highlightPane').style.zIndex = 650;

    this.orthoLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'orthofotos_swissimage_2018_group',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        useCache: true
    });
    this.grundLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'grundkarte_sw_group',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        useCache: true
    });
    this.kurvLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'hoehenkurven_2018_raster',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        format: 'image/png',
        transparent: true,
        useCache: true
    });
    this.baseLayers = [this.grundLayer, this.kurvLayer];
    this.baseLayers.forEach(lay => lay.addTo(this.map));
    // Initially empty layers
    this.inventoryLayer = L.geoJSON().addTo(this.map);
    this.clusterLayer = L.markerClusterGroup({
        iconCreateFunction: function(cluster) {
            /* Cluster color: If at least one is red, cluster is red */
            let col = 'green';
            cluster.getAllChildMarkers().forEach(item => {
                if (col == 'red') return;
                let itemCol = item.options.icon.options.className.split('-').pop();
                if (itemCol == 'red') col = 'red';
                else if (itemCol == 'orange') col = 'orange';
            });
            return L.divIcon({
                html: '<div><span>' + cluster.getChildCount() + '</span></div>',
                className: 'marker-cluster marker-cluster-' + col,
                iconSize: new L.Point(40, 40)
            });
        }
    }).addTo(this.map);

    L.control.scale({imperial: false}).addTo(this.map);
};

Map.prototype.attachHandlers = function () {
    var self = this;
    this.map.on('moveend', function() {
        // Store current center and zoom level
        var zoom = self.map.getZoom(),
            center = self.map.getCenter();
        db.get('current-state').then(currentState => {
            var changed = false;
            if (currentState.zoom !== zoom) {
                currentState.zoom = zoom;
                changed = true;
            }
            if (!center.equals(currentState.center)) {
                currentState.center = center;
                changed = true;
            }
            if (changed) {
                db.put(currentState).catch(err => {
                    // Ignoring any error, as this is not critical;
                    console.log("Non-fatal error: " + err);
                    return;
                });
            }
        }).catch(err => {
            if (err.name === 'not_found') {
                db.put({_id: 'current-state', zoom: zoom, center: center});
            } else throw err;
        });
    });
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(position => {
                self.setCurrentPosition(position);
            }, error => {
                console.log("Geolocation error: " + error.message);
            },
            {timeout: 10000, enableHighAccuracy: true, maximumAge: 5000
        });
    }
};

Map.prototype.setCurrentPosition = function (position) {
    if (!this.currentPositionLayer) {
        this.currentPositionLayer = L.marker(
            [0, 0], {
            icon: L.icon({iconUrl: staticImages.currentPos, iconSize: [20, 20]}),
            opacity: 0.9
        });
        this.map.addLayer(this.currentPositionLayer);
    }
    this.currentPositionLayer.setLatLng([
        position.coords.latitude,
        position.coords.longitude
    ]);
};

Map.prototype.switchBaseLayer = function () {
    this.baseLayers.forEach(lay => this.map.removeLayer(lay));
    this.baseLayers = this.baseLayers[0] === this.orthoLayer ? [this.grundLayer, this.kurvLayer] : [this.orthoLayer];
    this.baseLayers.forEach(lay => lay.addTo(this.map));
};

Map.prototype.toCurrentPosition = function () {
    if (this.currentPositionLayer) {
        var newPos = this.currentPositionLayer.getLatLng();
        this.map.setView(newPos, Math.max(this.map.getZoom(), 16));
    } else {
        modal.show("Ihr Standort konnte nicht ermittelt werden.");
    }
};

Map.prototype.loadLayer = function (layerName, url) {
    var self = this;
    loadJSON(url).then(geojson => {
        if (self.vLayers[layerName]) {
            self.vLayers[layerName].clearLayers();
            self.vLayers[layerName].addData(geojson);
        } else {
            self.vLayers[layerName] = L.geoJSON(geojson, {
                style: styles[layerName],
                onEachFeature: onEachFeature
            });
            self.vLayers[layerName].addTo(self.map);
        }
    });
};

/*
 * Store map data offline.
 * Vector data are already automatically cached through serviceworker.js.
 */
Map.prototype.storeOffline = function () {
    var bbox = this.map.getBounds(),
        zoomCurrent = this.map.getZoom(),
        zoomMax = this.grundLayer.options.maxNativeZoom;
    if (zoomMax - zoomCurrent > 4) {
        modal.show("Sie müssen einen kleineren Bereich auswählen um die Karte Daten lokale zu speichern.");
        return;
    }

    document.getElementById('progress').style.display = 'block';
    progressBar = new ProgressBar.Line('#progress', {
        color: '#008000', strokeWidth: 5
    });
    this.grundLayer.on('seedprogress', function(seedData){
        //var percent = 100 - Math.floor(seedData.remainingLength / seedData.queueLength * 100);
        progressBar.set(progressBar.value() + (1 / seedData.queueLength));
    });
    this.grundLayer.on('seedend', function(seedData){
        progressBar.destroy();
        progressBar = null;
        document.getElementById('progress').style.display = 'none';
        modal.show("Daten wurden erfolgreich gespeichert");
    });

    /*
     * Some figures about tiles caching:
     * tile size: 256x256
     * tile download size: 25Kb
     * tile size in db: (Base64), ~220Kb??
     * on one map level: 12 tiles
     * seeding:
     *   - 1 level: 42/48 tiles (1.1Mb dl / 10Mb stor.)
     *   - 2 levels: 140/150 tiles (3.6Mb dl / 35Mb stor.)
     *   - 3 levels: ~500 tiles (12.5Mb dl / 120Mb stor.)
     *   - 4 levels: ~2000 tiles (50Mb dl / 500Mb stor.)
     * Browser Storage docs:
     *  https://medium.com/dev-channel/offline-storage-for-progressive-web-apps-70d52695513c#.tva434uxh
     */
    this.grundLayer.seed(bbox, zoomCurrent, zoomMax);
    // Progress/end of seeding is handled in 'seedprogress'/'seedend' events
}

/*
 * Plot class
 */
class Plot {
    constructor(feature) {
        this.id = feature.properties.pk;
        this.geoJSON = feature;
        this.properties = feature.properties;
        this.layer = null;
        // Convert objects to Massnahme instances.
        this.properties.unterhalt_set.forEach((item, idx) => {
            this.properties.unterhalt_set[idx] = new Massnahme(item);
        });
    }

    center() { return this.geoJSON.geometry; }
    serialize() { return this.geoJSON; }
    dbId() { return 'einrichtung-' + this.id; }

    showOnMap(map) {
        if (this.layer !== null) {
            try {
                map.clusterLayer.removeLayer(this.layer);
            } catch(err) {
                console.log(err);
            }
        }
        const coords = this.center().coordinates.slice().reverse();
        const iconClass = 'icon-plot-' + this.colorStatus();
        const icon = L.divIcon({className: iconClass, iconSize: [20, 20], iconAnchor: [10, 10]});
        this.layer = L.marker(coords, {icon: icon});
        var self = this;
        this.layer.on({
            click: function (ev) {
                const current = document.getElementsByClassName('icon-selected');
                if (current.length) {
                    current[0].classList.remove('icon-selected');
                }
                self.layer._icon.classList.add('icon-selected');
                currentPlot = self;
                self.showDetails();
            },
            moveend: function (ev) {
                const newPos = ev.target.getLatLng();
                document.getElementById('id_new_lon').value = newPos.lng;
                document.getElementById('id_new_lat').value = newPos.lat;
                // The cluster layer will remove and re-add the layer, losing selected style
                setTimeout(() => self.layer._icon.classList.add('icon-selected'), 300);
            }
        });
        map.clusterLayer.addLayer(this.layer);
    }

    showDetails() {
        show('#infos-div');
        einForm.fillFrom(this);
        this.showMassnahmen();
        this.layer.dragging.disable();
    }

    colorStatus() {
        // Check all massnahmen:
        // if none from this year -> red,
        // else if all of this year erledigt or keyin_unterhalt -> green
        // else orange
        const thisYear = new Date().getFullYear();
        let anyThisYear = false;
        let allOK = true;
        this.properties.unterhalt_set.forEach((item, idx) => {
            if (item.properties.datum.getFullYear() != thisYear) return;
            anyThisYear = true;
            allOK = allOK && (item.properties.erledigt || item.properties.kein_unterhalt);
        });
        if (!anyThisYear) return 'red';
        else if (allOK) return 'green';
        return 'orange';
    }

    showMassnahmen() {
        // Fill massnahme list
        const massUL = document.getElementById('mass-list');
        show(massUL);
        while (massUL.firstChild) {
            massUL.removeChild(massUL.firstChild);
        }
        this.properties.unterhalt_set.forEach(item => {
            let li = item.asLI();
            massUL.appendChild(li);
            li.addEventListener('click', function () {item.edit()});
        });
    }

    edit() {
        this.layer.dragging.enable();
    }

    save(form) {
        var self = this;
        if (form) {
            var objData = formToObject(form);
            // Save data to self
            for (const [key, value] of Object.entries(objData)) {
                self.geoJSON.properties[key] = value;
            }
        } else {
            var objData = Object.assign({}, self.geoJSON.properties);
            delete objData.unterhalt_set;
        }
        if (isOnline()) {
            // Get values from form and POST to server
            const options = {
                method: 'POST',
                body: objToFormData(objData),
            }
            const url = document.getElementById('einrichtung-form').action;
            options.body.append('id', self.id);
            return fetch(url, options)
                .then(res => res.json())
                .then(resp => {
                    if (resp.result == 'OK') {
                        return resp.obj;
                    } else throw resp.errors;
                }).catch((err) => {
                    console.log(err);
                    return saveLocally(self);
                });
        } else {
            return saveLocally(self);
        }
    }
}

class Massnahme {
    constructor(obj) {
        this.properties = {};
        for(const [key, value] of Object.entries(obj)) {
            this.set(key, value);
        }
        if (!this.properties.id) this.properties.id = 'auto-' + Date.now().toString();
    }

    set(key, val) {
        if ((key == 'datum' || key == 'erledigt') && typeof val === 'string' && val != '') {
            this.properties[key] = new Date(Date.parse(val));
        } else {
            this.properties[key] = val;
        }
    }

    dbId() {
        return 'massnahme-' + this.properties.id;
    }

    serialize() {
        const res = Object.assign({}, this.properties);
        res.datum = toISODate(this.properties.datum);
        res.erledigt = toISODate(this.properties.erledigt);
        return res
    }

    asLI() {
        let li = document.createElement("li");
        const dateFormat = {weekday: 'short', month: 'short', day: 'numeric'};
        const dt_str = this.properties.datum.toLocaleString('de-CH', dateFormat);
        let item_text = dt_str + ': ';
        if (this.properties.erledigt) {
            const erledigt = this.properties.erledigt.toLocaleString('de-CH', dateFormat);
            item_text += this.properties.massnahme_objekt + this.properties.massnahme_objekt ? ', ' : '' + 'erledigt am ' + erledigt;
            li.classList.add('erledigt');
        } else if (this.properties.kein_unterhalt) {
            item_text += '<i>Kein Unterhalt nötig</i>';
        } else {
            item_text += this.properties.massnahme_objekt;
            if (item_text.length && this.properties.massnahmen_baumbestand.length) item_text += ', ';
            item_text += this.properties.massnahmen_baumbestand;
        }
        li.innerHTML = item_text;
        return li;
    }

    edit() {
        hide('#mass-list');
        hide('button#new-massnahme');
        massForm.fillFrom(this);
        show(massForm.form);
        show(massForm.form.querySelector('.save-line'));
    }

    save(form) {
        var formData = new FormData();
        for (const [key, value] of Object.entries(this.serialize())) {
            formData.append(key, value);
        }
        return postForm(null, {
            formData: formData,
            formAction: document.getElementById('massnahme-form').action
        }).then(resp => {
            if (resp.result == 'OK') {
                this.properties = resp.obj;
            } else throw resp.errors;
        });
    }
}

var EinrichtungForm = function () {
    this.form = document.getElementById('einrichtung-form');
    this.attachHandlers();
};

EinrichtungForm.prototype.attachHandlers = function () {
    const self = this;
    document.getElementById('edit-plot').addEventListener('click', function (ev) {
        ev.preventDefault();
        hide(this.closest('tr'));  // Hide edit button
        show(this.closest('tr').nextElementSibling);  // Show save/cancel buttons
        Array.from(self.form.elements).forEach(
            el => el.disabled = false
        );
        currentPlot.edit(self);
    });
    this.form.querySelector('button.cancel').addEventListener('click', function (ev) {
        ev.preventDefault();
        currentPlot.showDetails();
    });
    this.form.querySelector('button.save').addEventListener('click', function (ev) {
        ev.preventDefault();
        currentPlot.save(self.form).then(() => {
            hide(this.closest('tr'));
            show(this.closest('tr').previousElementSibling);
        });
        Array.from(self.form.elements).forEach(
            el => { if (el.tagName != 'BUTTON') el.disabled = true; }
        );
    });
};

EinrichtungForm.prototype.fillFrom = function (plot) {
    show('#edit-line');
    hide(this.form.querySelector('.save-line'));

    // Fill form values
    const props = plot.properties;
    Object.keys(props).forEach((key, index) => {
        const input = document.getElementById(`id_${key}`);
        if (input && input.type != 'file') {
            if (isArray(props[key])) input.value = props[key][0];
            else input.value = props[key];
            input.disabled = true;
        }
    });
    document.getElementById('id_new_lon').value = '';
    document.getElementById('id_new_lat').value = '';
}

var MassnahmeForm = function () {
    this.form = document.getElementById('massnahme-form');
    this.currentMassnahme = null;
    this.attachHandlers();
};

MassnahmeForm.prototype.attachHandlers = function () {
    const self = this;
    document.getElementById('new-massnahme').addEventListener('click', (ev) => {
        ev.preventDefault();
        hide('button#new-massnahme');
        hide('#mass-list');
        show(self.form);
        show(self.form.querySelector('.save-line'));
        // Reset massnahme form to default values
        self.form.reset()
        self.form.elements.id_datum.value = new Date().toISOString().slice(0, 10);
        self.form.elements.id_objekt.value = currentPlot.id;
        self.currentMassnahme = null;
    });
    self.form.querySelector('button.cancel').addEventListener('click', (ev) => {
        ev.preventDefault();
        hide(self.form);
        show('#mass-list');
        show('#new-massnahme');
    });
    self.form.querySelector('button.save').addEventListener('click', (ev) => {
        ev.preventDefault();
        const isNew = self.currentMassnahme === null;
        self.save().then(obj => {
            if (obj instanceof Massnahme) var newM = obj;
            else var newM = new Massnahme(obj);
            if (isNew) currentPlot.properties.unterhalt_set.unshift(newM);
            else {
                const idx = currentPlot.properties.unterhalt_set.findIndex(
                    m => m.properties.id === self.currentMassnahme.properties.id
                );
                if (idx >= 0) currentPlot.properties.unterhalt_set[idx] = newM;
            }
            hide(self.form);
            self.currentMassnahme = null;
            currentPlot.showMassnahmen();
            currentPlot.showOnMap(map);  // Point color may have changed
            show('#new-massnahme');
        }).catch(errs => {
            let txt = '';
            if (errs instanceof String) txt = errs;
            else {
                Object.entries(errs).forEach(item => {
                    txt += item[0] + ': ' + item[1][0] + '\n';
                });
            }
            if (!txt.length) txt = "Beim Speichern ist ein unbekannter Fehler aufgetreten";
            modal.show(txt);
        });
    });
}

MassnahmeForm.prototype.fillFrom = function (massn) {
    Object.keys(massn.properties).forEach((key, index) => {
        const input = document.getElementById(`id_${key}`);
        if (input) {
            if (isArray(massn.properties[key])) input.value = massn.properties[key][0];
            else if (input.type == 'date') {
                if (massn.properties[key]) input.value = toISODate(massn.properties[key]);
            } else if (input.type == 'checkbox') {
                input.checked = massn.properties[key];
            } else input.value = massn.properties[key];
        }
    });
    this.currentMassnahme = massn;
}

MassnahmeForm.prototype.save = function () {
    const formData = new FormData(this.form);
    if (isOnline()) {
        let extra = {};
        if (this.currentMassnahme) extra['id'] = this.currentMassnahme.properties.id;
        return postForm(this.form, {extraPost: extra}).then(resp => {
            if (resp.result == 'OK') return resp.obj;
            else throw resp.errors;
        });
    } else {
        if (this.currentMassnahme) {
            for (const [key, value] of formData.entries()) {
                this.currentMassnahme.set(key, value);
            }
            var massnahme = this.currentMassnahme;
        } else {
            // create new object from Form
            var newObj = {};
            for(const [key, value] of formData.entries()) {
                newObj[key] = value;
            }
            var massnahme = new Massnahme(newObj);
        }
        return saveLocally(massnahme);
    }
}

class Modal {
    constructor() {
        this.modalDiv = document.getElementById("modal-div");
        this.buttonJa = this.modalDiv.querySelector('#modal-ja');
        this.buttonNein = this.modalDiv.querySelector('#modal-nein');
        const self = this;
        this.modalDiv.querySelector('.modal-close').addEventListener('click', function (ev) {
            self.hide();
        });
        this.modalDiv.querySelector('#modal-ok').addEventListener('click', function (ev) {
            self.hide();
        });
        this.modalDiv.querySelector('#modal-ja').addEventListener('click', function (ev) {
            self.hide();
            return
        });
        this.modalDiv.querySelector('#modal-nein').addEventListener('click', function (ev) {
            self.hide();
        });
    }
    hide(ev) { this.modalDiv.classList.add('hidden'); }
    show(msg) {
        this.modalDiv.querySelector('#modal-content').innerText = msg;
        this.modalDiv.classList.remove('hidden');
    }
}

var modal = new Modal();
/*
 * Number widget to reflect the number of unsynchronised PlotObs.
 */
var UnsyncCounter = function () {
    this.count = 0;
    this.div = document.getElementById('unsync-counter');
    var self = this;
    db.get("new-plotobs").then(doc => {
        self.setCount(doc.newIDs.length);
    }).catch(err => {
        self.setCount(0);
    });
    // Allow force-resync of all local ksp data
    this.div.addEventListener('dblclick', checkOfflineData);
};

UnsyncCounter.prototype.setCount = function (count) {
    if (count < 0) count = 0;
    this.count = count;
    this.div.innerHTML = this.count;
    if (this.count > 0) {
        this.div.classList.add("nonempty");
    } else {
        this.div.classList.remove("nonempty");
    }
};

UnsyncCounter.prototype.increment = function () { this.setCount(this.count + 1); }
UnsyncCounter.prototype.decrement = function () { this.setCount(this.count - 1); }

var map = new Map();

function saveData(docId, data) {
    // Save data in local database, replacing any existing occurrences
    db.get(docId).then(doc => {
        return db.remove(doc);
    }).then(result => {
        data._id = docId;
        return db.put(data);
    }).catch(err => {
        if (err.name === 'not_found') {
            data._id = docId;
            return db.put(data);
        } else throw err;
    });
}

function saveLocally(obj) {
    var id = obj.dbId();
    saveData(id, obj.serialize());
    // Save the id for future sync
    return db.get('new-plotobs').catch(err => {
        if (err.name === 'not_found') {
            return {
                _id: 'new-plotobs',
                newIDs: []
            };
        }
    }).then(doc => {
        if (doc.newIDs.indexOf(id) < 0) {
            doc.newIDs.push(id);
            unsyncCounter.increment();
            return db.put(doc);
        }
    }).then(() => { return obj; });
}

function postForm(form, opts) {
    var formData = form ? new FormData(form) : opts.formData;
    const fetchOptions = {
        method: 'POST',
        body: formData,
    }
    for (const key in opts.extraPost || {}) {
        fetchOptions.body.append(key, opts.extraPost[key]);
    }
    return fetch(opts.formAction || form.action, fetchOptions)
        .then(res => res.json())
}

function loadInventoryData() {
    map.clusterLayer.clearLayers();
    return loadJSON(inventoryURL).then(function(features) {
        for (let i = 0; i < features.features.length; i ++) {
            const plot = new Plot(features.features[i]);
            plot.showOnMap(map);
        }
    });
}

function checkOfflineData() {
    if (!window.navigator.onLine) return;
    // If unsynced content, propose to sync
    db.get('new-plotobs').then(doc => {
        if (doc.newIDs.length == 0) return;
        var promises = [];
        // Get all locally saved ids
        for (var i=0; i < doc.newIDs.length; i++) {
            var docId = doc.newIDs[i];
            var p = db.get(docId).then(podata => {
                if (podata._id.includes('einrichtung')) {
                    var obj = new Plot(podata);
                } else {
                    var obj = new Massnahme(podata);
                }
                obj._dbid = docId;
                return obj;
            }).catch(err => {
                console.log(err);
                console.log("Unable to retrieve local Plot with id " + docId);
            });
            promises.push(p);
        }
        Promise.all(promises).then(pos => {
            pos = pos.filter(po => po !== undefined); // undefined in case of errors
            if (!pos.length) return;
            if (!confirm("Möchten sie jetzt die neue offline Daten synchronisieren?")) return;
            var promises2 = [];
            const errors = [];
            for (var i=0; i < pos.length; i++) {
                var this_po = pos[i];
                var p = this_po.save(null).then(() => {
                    unsyncCounter.decrement();
                    // Remove the id from the newIDs list
                    db.get('new-plotobs').then(doc => {
                        doc.newIDs.splice(doc.newIDs.indexOf(this_po._dbid), 1);
                        db.put(doc);
                    });
                }).catch(err => {
                    errors.push(err);
                });
                promises2.push(p);
            }
            Promise.all(promises2).then(() => {
                loadInventoryData();
                let msg = "Die Daten sind jetzt auf dem Server gespeichert."
                if (errors.length) msg += "\nLeider gab es Server Fehler. Bitte versuchen Sie es später noch einmal.";
                modal.show(msg);
            });
        });
    }).catch(err => {
        // A not_found indicates we have no unsynced content
        if (err.name !== 'not_found') {
            console.log(err);
        }
    });
}

function setInitialState() {
    db.get('current-state').then(data => {
        map.map.setView(data.center, data.zoom);
    }).catch(err => {
        map.map.setView(CENTER_INITIAL, ZOOM_INITIAL);
    });
    loadInventoryData();
}

document.addEventListener("DOMContentLoaded", function(event) { 
    redirect_log_messages();
    unsyncCounter = new UnsyncCounter();

    document.getElementById('sync-unsync').addEventListener('click', function (ev) {
        if (!confirm("Möchten sie jetzt auf der Karte sichtbaren Daten lokal speichern?")) return;
        map.storeOffline();
    });

    function goingOnline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "block";
        document.getElementById('logout').style.display = "block";
        checkOfflineData();
    }

    function goingOffline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "none";
        document.getElementById('logout').style.display = "none";
    }
    if (!window.navigator.onLine) toggleImg('network-status');
    window.addEventListener('online',  goingOnline);
    window.addEventListener('offline', goingOffline);

    map.attachHandlers();
    // Base layer switcher
    document.getElementById('baselayer').addEventListener('click', function (ev) {
        toggleImg('baselayer');
        map.switchBaseLayer();
    });
    document.getElementById('tocurrent').addEventListener('click', function (ev) {
        map.toCurrentPosition();
    });
    einForm = new EinrichtungForm();
    massForm = new MassnahmeForm();

    const versionDiv = document.getElementById('appversion');
    versionDiv.innerHTML = 'v.' + AppVersion;
    versionDiv.addEventListener('dblclick', function (ev) {
        document.querySelector('#debug-pane').classList.remove('hidden');
    });
    const reloader = document.querySelector('#debug-reload');
    reloader.addEventListener('click', function (ev) {
        caches.open(CACHE_NAME).then(function(cache) {
            cache.keys().then(function(keys) {
                keys.forEach(function(request, index, array) {
                    cache.delete(request);
                });
            });
        });
        window.location.reload(true);
    });
    document.querySelectorAll('#debug-pane .close, #infos-div .close').forEach(item => {
        item.addEventListener('click', function (ev) {
            document.querySelector('#' + this.dataset.target).classList.add('hidden');
        });
    });
    checkOfflineData();
    setInitialState();
});

// Debug function to clear locally saved data
function clear_local_data() {
    db.get('new-plotobs').catch(err => {
        return;
    }).then(doc => {
        doc.newIDs = [];
        return db.put(doc);
    });
    unsyncCounter.setCount(0);
}

function redirect_log_messages() {
    var origLog = console.log;
    var logger = document.querySelector('#debug-messages');
    console.log = function (message) {
        if (typeof message == 'object') {
            try {
                logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br>';
            } catch (err) {
                logger.innerHTML += message + ' (stringify failed)<br>';
            }
        } else {
            logger.innerHTML += message + '<br>';
        }
        origLog(message);
    }
}

    
