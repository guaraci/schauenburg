from django import forms
from django.contrib.gis.geos import Point

from unterhalt.forms import UnterhaltForm as UnterhaltFormBase
from unterhalt.models import Freizeiteinrichtung


class DateInput(forms.DateInput):
    input_type = 'date'


class FreizeitForm(forms.ModelForm):
    new_lon = forms.CharField(widget=forms.HiddenInput, required=False)
    new_lat = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Freizeiteinrichtung
        exclude = ['laufender_unterhalt', 'periodischer_unterhalt', 'foto', 'the_geom']

    def save(self, **kwargs):
        if self.cleaned_data['new_lon'] and self.cleaned_data['new_lat']:
            self.instance.the_geom = Point(
                x=float(self.cleaned_data['new_lon']), y=float(self.cleaned_data['new_lat']), srid=4326
            )
        return super().save(**kwargs)


class UnterhaltForm(UnterhaltFormBase):
    class Meta(UnterhaltFormBase.Meta):
        widgets = {
            'objekt': forms.HiddenInput,
            'datum': DateInput,
            'erledigt': DateInput,
            'foto': forms.ClearableFileInput(attrs={'accept': "image/*"}),
        }
