from django.urls import include, path
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.staticfiles.views import serve as static_serve
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from django.views.i18n import JavaScriptCatalog

from . import views

urlpatterns = [
    path('login/', LoginView.as_view(redirect_authenticated_user=True, template_name='mobile/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),

    path('', views.MainView.as_view(), name='home'),
    path('jsi18n/', cache_page(86400, key_prefix='jsi18n-1')(JavaScriptCatalog.as_view()),
         name='javascript-catalog'),
    path('inventory/', views.InventoryView.as_view(), name='inventory'),
    path('save-einrichtung/', csrf_exempt(views.SaveEinrichtungView.as_view()), name='save-einrichtung'),
    path('save-masshahme/', csrf_exempt(views.SaveMassnahmeView.as_view()), name='save-massnahme'),
    path('sync/', csrf_exempt(views.SyncView.as_view()), name='sync'),

    path('serviceworker.js', static_serve, kwargs={
        'path': 'js/serviceworker.js', 'insecure': True}),
]
