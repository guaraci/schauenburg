import json
from datetime import date, timedelta

from django.contrib.gis.serializers.geojson import Serializer as GeoJSONSerializer
from django.db.models import Prefetch
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, View

from unterhalt.models import Freizeiteinrichtung, Unterhalt
from .forms import FreizeitForm, UnterhaltForm


class FreizeitSerializer(GeoJSONSerializer):
    def serialize_unterhalt(self, unt):
        data = {
            field.name: getattr(unt, field.name)
            for field in unt._meta.get_fields() if field.name not in ('foto')
        }
        data['objekt'] = unt.objekt_id
        return data

    def handle_fk_field(self, obj, field):
        raw_val = self._value_from_field(obj, field)
        self._current[field.name] = (raw_val, str(getattr(obj, field.name)) if raw_val else None)

    def get_dump_object(self, obj):
        data = super().get_dump_object(obj)
        this_year = date.today().year
        data['properties']['unterhalt_set'] = [
            self.serialize_unterhalt(unt)
            for unt in obj.unterhalt_set.all() if unt.datum.year >= this_year - 1
        ]
        return data


class MainView(TemplateView):
    template_name = 'mobile/index.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'main_form': FreizeitForm(),
            'mass_form': UnterhaltForm(),
        }


class SyncView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        # TODO synchronize input content
        return JsonResponse({})


class SaveEinrichtungView(View):
    def post(self, request, *args, **kwargs):
        existing = get_object_or_404(Freizeiteinrichtung, pk=request.POST.get('id'));
        form = FreizeitForm(data=request.POST, instance=existing)
        if form.is_valid():
            form.save()
        else:
            return JsonResponse({'result': 'error', 'errors': form.errors})
        ser_ctxt = FreizeitSerializer()
        return JsonResponse({'result': 'OK', 'obj': ser_ctxt.serialize([form.instance], srid=4326)})


class SaveMassnahmeView(View):
    def post(self, request, *args, **kwargs):
        obj_id = request.POST.get('id')
        try:
            obj_id = int(request.POST.get('id'))
            existing = get_object_or_404(Unterhalt, pk=obj_id);
        except (ValueError, TypeError):
            existing = None
        form = UnterhaltForm(data=request.POST, files=request.FILES, instance=existing)
        if form.is_valid():
            # Avoid setting 00:00 hour, because timezone could shift to previous day.
            form.instance.datum += timedelta(hours=10)
            form.save()
        else:
            return JsonResponse({'result': 'error', 'errors': form.errors})
        ser_ctxt = FreizeitSerializer()
        return JsonResponse({'result': 'OK', 'obj': ser_ctxt.serialize_unterhalt(form.instance)})


class InventoryView(View):
    """This view returns a GeoJSON with Inventory data."""
    def get(self, request, *args, **kwargs):
        objs = Freizeiteinrichtung.objects.all().prefetch_related('unterhalt_set')
        ser_ctxt = FreizeitSerializer()
        ser_ctxt.serialize(objs, srid=4326)
        return JsonResponse(json.loads(ser_ctxt.getvalue()))
