from django import forms, template
from django.urls import reverse
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag(takes_context=True)
def breadcrumb(context):
    if context.get('on_home'):
        return ''
    txt = '<a href="%s" title="zurück zur Übersicht">Home</a>' % reverse('home')
    bc = context.get('breadcrumb', [])
    for entry in bc:
        txt = '%s &gt; <a href="%s">%s</a>' % (txt, entry[1], entry[0])
    title = context.get('title')
    if title:
        txt = '%s &gt; %s' % (txt, title)
    return mark_safe(txt)


@register.filter
def is_checkbox(field):
    return isinstance(field.field.widget, forms.CheckboxInput)


@register.filter(is_safe=True)
def strip_colon(label):
    return label.replace(":</label>", "</label>")
