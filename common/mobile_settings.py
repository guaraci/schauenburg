from .settings import *

ROOT_URLCONF = 'mobile.urls'
# Avoid using e.g. 404.html with missing urls
TEMPLATES[0]['DIRS'] = []

INSTALLED_APPS += ['mobile']

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
