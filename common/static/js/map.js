'use strict';

var epsg2056Converter = proj4(
    proj4('EPSG:4326'), proj4('EPSG:2056')
);

// Needed to center the map when no vector objects are displayed
const absCenter = [2618000, 1262000];
const ZOOM_INITIAL = 18;
const geojsonMarkerOptions = {
    radius: 10,
    fillColor: "#ff7800",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};

function rounded(num, dec) { return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec); }

/*
 * Map class
 */
class Map {
    constructor(layerCodes) {
        this.map = L.map('map', {crs: L.CRS.EPSG2056});
        this.map.createPane('highlightPane');
        this.map.getPane('highlightPane').style.zIndex = 650;
        this.orthoLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'orthofotos_swissimage_2018_group',
            minZoom: 18,
            maxZoom: 28,
            useCache: true
        });
        this.planLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'grundkarte_sw_group',
            minZoom: 18,
            maxZoom: 26,
            useCache: true
        });
        // A custom layer generated on the server by mapscript
        this.localLayer = L.tileLayer.wms('https://schauenburg.guaraci.ch/cgi-bin/schauenburg?', {
            layers: 'ortho',
            minZoom: 16,
            maxZoom: 28
        });
        this.baseLayers = layerCodes.map(code => this[code + 'Layer'], this);
        this.baseLayers.forEach(lay => lay.addTo(this.map));
        this.map.setView(L.CRS.EPSG2056.unproject(L.point(absCenter)), ZOOM_INITIAL);
        this.selectedMarker = null;
        const opacity_el = document.getElementById("layeropacity");
        if (opacity_el) {
            const self = this;
            opacity_el.addEventListener("input", (ev) => {
                self.update_opacity(ev.target.value);
            });
        }
    }

    loadFeatures(geojson, options, fitBounds, selectCallback) {
        if (geojson.features.length < 1) {
            console.log("Geojson structure has no features.");
            return;
        }
        const self = this;
        const opts = Object.assign({}, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, options.circleMarkerStyle || geojsonMarkerOptions);
            },
            onEachFeature: function(feature, layer) {
                if (selectCallback !== undefined) {
                    layer.on('click', function (ev) {
                        selectCallback(ev, self, this);
                    });
                }
            }
        }, options);
        let vectorLayer = L.geoJSON(geojson, opts);
        vectorLayer.addTo(this.map);
        if (fitBounds) {
            this.map.fitBounds(vectorLayer.getBounds());
        }
        return vectorLayer;
    }

    loadFeaturesFromURL(url, options, fitBounds, selectCallback) {
        const self = this;
        return fetch(url).then(resp => resp.json()).then(data => {
            if (data.features.length > 0) {
                return self.loadFeatures(data, options, fitBounds, selectCallback);
            }
            return null;
        });
    }

    followPosition() {
        const self = this;
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(function (position) {
                    self.setCurrentPosition(position);
                }, function(error) {
                    console.log("Geolocation error: ", error.code, error.message);
                },
                {timeout: 10000, enableHighAccuracy: true, maximumAge: 5000
            });
        }
    }

    setCurrentPosition(position) {
        // Diff from my position in CdF (debugging help)
        var devDiff = position.coords.longitude < 7 ? [0.41, 0.85] : [0, 0];
        if (!this.currentPositionLayer) {
            this.currentPositionLayer = L.marker(
                [0, 0], {
                icon: L.icon({iconUrl: staticImages.currentPos, iconSize: [20, 20], className: 'current-pos'}),
                opacity: 0.9
            });
            this.map.addLayer(this.currentPositionLayer);
        }
        this.currentPositionLayer.setLatLng([
            position.coords.latitude + devDiff[0],
            position.coords.longitude + devDiff[1]
        ]);
        var projected = epsg2056Converter.forward([
            position.coords.longitude + devDiff[1],
            position.coords.latitude + devDiff[0]]
        );
    }

    update_opacity(value) { this.baseLayers[1].setOpacity(1 - value); }

    selectLayer(lyr) {
        if (this.selectedMarker) {
            this.selectedMarker.setStyle(this.selectedMarker.savedStyle);
        }
        lyr.savedStyle = Object.assign({}, lyr.options);
        lyr.setStyle({'color': '#C4201F', 'fillColor': '#E4E400'});
        this.selectedMarker = lyr;
    }

    highlightFeature(featId) {
        const map = this.map;
        map.eachLayer(function (layer) {
            if (layer.feature && layer.feature.id == featId) {
                layer.fireEvent('click');
                // recenter map
                map.setView(layer.getLatLng(), 25);
            }
        });
    }
    unhighlightFeature(featId) {}
}

/* If the map was initiated by a leaflet widget, the layers will be empty as
*  we don't want the django-leaflet layer machinery (tileLayer hardcoded).
*  Therefore, we detect that in map:init and setup the layers there.
*/
window.addEventListener("map:init", function (ev) {
    var map = ev.detail.map;
    if (!map.layers) {
        var planLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'grundkarte_sw_group',
            minZoom: 18,
            maxZoom: 26,
            useCache: true
        });
        planLayer.addTo(map);
        map.setView(L.CRS.EPSG2056.unproject(L.point(absCenter)), ZOOM_INITIAL);
    }
});
