$(document).ready(function() {
    // Generic form submit handler to display any confirm alert
    $("body").on('submit', 'form', function(ev) {
        if ($(this).data('confirm')) {
            if (!confirm($(this).data('confirm'))) {
                ev.preventDefault();
                return False;
            }
        }
    });
});
