import re

from django.conf import settings
from django.http import HttpResponseRedirect

EXEMPT_URLS = [
    re.compile(settings.LOGIN_URL.lstrip('/')),
    re.compile('static/*'),
    re.compile('public/*'),
]


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user.is_authenticated:
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect("%s?next=%s" % (settings.LOGIN_URL, request.path_info))
        return self.get_response(request)
