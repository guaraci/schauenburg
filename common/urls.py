from django.conf import settings
from django.contrib.auth import views as auth_views
from django.contrib.gis import admin
from django.urls import include, path
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve

from massnahmen.views import IndexView


urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('jsi18n/', cache_page(86400, key_prefix='jsi18n-1')(JavaScriptCatalog.as_view()),
         name='javascript-catalog'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('unterhalt/', include('unterhalt.urls')),
    path('waldrand/', include('waldrand.urls')),
    path('public/', include('public.urls')),
    path('', include('massnahmen.urls')),

    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += [
        path('media/<path:path>', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
