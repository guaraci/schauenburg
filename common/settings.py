# Django settings for schauenburg project.
import os

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

ADMINS = (
    ('Claude Paroz', 'claude@2xlibre.net'),
)

MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = 'webmaster@2xlibre.net'
SERVER_EMAIL = 'webmaster@2xlibre.net'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

TIME_ZONE = 'Europe/Zurich'

LANGUAGE_CODE = 'de'

USE_I18N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, 'common/static'),
)

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'common.middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'common.urls'

LOGIN_URL = '/login/'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'common.wsgi.application'

TEMPLATES = [{
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(PROJECT_PATH, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        }
    },
]


INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.gis',
    'leaflet',

    'common',
    'bestand',
    'massnahmen',
    'unterhalt',
    'waldrand',
    'public',
]

MAPFILE_PATH = '/home/raphi/karten/schauenburg.map'

# ~= [2617000, 1260000],
DEFAULT_LON = 7.68
DEFAULT_LAT = 47.52
LEAFLET_CONFIG = {
    'TILES': [],  # We set the base layer ourselves
    'SRID': 2056,
    'DEFAULT_CENTER': (DEFAULT_LAT, DEFAULT_LON),
    'DEFAULT_ZOOM': 18,
    'MIN_ZOOM': 18,
    'MAX_ZOOM': 26,
    'SPATIAL_EXTENT': (5, 40, 8, 50),
}

from .local_settings import *
