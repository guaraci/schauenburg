#!/usr/bin/env python

import argparse
import mapscript
import sys
import tempfile


def map_image(args):
    """
    Produce image through mapscript, http://mapserver.org/mapscript/python.html#python
    """
    map_ = mapscript.mapObj(args.map_config)

    # Disable all layers except the one we want (from args)
    for i in range(map_.numlayers):
        lay = map_.getLayer(i)
        if lay.name not in ('ortho', args.layer, args.map_name):
            lay.status = mapscript.MS_OFF
        elif lay.name == args.layer:
            # Select only the shape we want
            lay.addProcessing('NATIVE_FILTER=%s=%d' % (args.pkfield, args.pk))
        elif lay.name == args.map_name:
            lay.status = mapscript.MS_ON
    map_.scalebar.status = mapscript.MS_EMBED
    map_.scalebar.units = mapscript.MS_METERS
    map_.scalebar.style = 1
    map_.scalebar.width = 232
    map_.setSize(1000, int(1000 * args.rate))
    extent_x, extent_y = 1000 * args.scale, int(1000 * args.scale * args.rate)
    minx, maxx = args.centerx - (extent_x/2), args.centerx + (extent_x/2)
    miny, maxy = args.centery - (extent_y/2), args.centery + (extent_y/2)
    map_.setExtent(minx, miny, maxx, maxy)
    img = map_.draw()
    tmp_img = tempfile.NamedTemporaryFile(suffix='.png', delete=False)
    img.save(tmp_img.name)
    return tmp_img.name

if __name__ == "__main__":
    # arg example:
    # --map-config=/path/to/schauenburg.map --rate=0.5 --scale=1.8 --centerx=619119.4 --centery=261372.1 --layer=planung --pk=60 --pkfield=gid
    parser = argparse.ArgumentParser(description='Create an image from mapserver.')
    parser.add_argument('--map-config', help="path to the mapserver .map config file")
    parser.add_argument('--map-name', help="name of the map to select")
    parser.add_argument('--layer', help="name of a layer to select")
    parser.add_argument('--pk', type=int, help="specific object to select")
    parser.add_argument('--pkfield', help="name of the primary key field")
    parser.add_argument('--rate', type=float, help="width/height ratio of the image")
    parser.add_argument('--scale', type=float, help="pixel/map ratio of the map")
    parser.add_argument('--centerx', type=float, help="x coordinate of the center")
    parser.add_argument('--centery', type=float, help="y coordinate of the center")
    args = parser.parse_args()
    # Print the image name to stdout
    sys.stdout.write(map_image(args))
    sys.exit(0)
