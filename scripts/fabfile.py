import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'pgadmin.guaraci.ch'

project = 'schauenburg'
default_db_owner = 'claude'
default_db_name = 'bep_fr_schauenburg'


@task(hosts=[MAIN_HOST])
def deploy(conn):
    python_exec = "/var/www/virtualenvs/%s3/bin/python" % project

    with conn.cd("/var/www/%s" % project):
        # activate maintenance mode
        conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" common/wsgi.py')
        conn.run('git stash && git pull && git stash pop')
        conn.run('%s manage.py migrate' % python_exec)
        conn.run('%s manage.py collectstatic --noinput --settings=common.mobile_settings' % python_exec)
        conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" common/wsgi.py')


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname=default_db_name):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_username(db):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return db in res.stdout.split()

    def exist_local_db(db):
        res = local.run('psql --list', hide=True)
        return db in res.stdout.split()

    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.run(f'touch {dbname}.sql && chmod o+rw {dbname}.sql')
    conn.sudo(f'pg_dump -Fc --no-owner --no-privileges {dbname} > {dbname}.dump', user='postgres')
    conn.get(f'{dbname}.dump')

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f'psql -d postgres -c "DROP DATABASE {dbname};"')
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    local.run(f'psql -d {dbname} -c "CREATE EXTENSION IF NOT EXISTS postgis;"')
    local.run(f'pg_restore -U {owner} -x --no-owner -d {dbname} {dbname}.dump')
